<?php

function response($status,$status_message,$data)
{
	header("HTTP/1.1 ".$status);
	
	$response['status']=$status;
	$response['message']=$status_message;
	$response['data']=$data;
	
	$json_response = json_encode($response);
	echo $json_response;
}

function logInUser($email) {
	$_SESSION["email"] = $email;
}

function startPresence($presence) {
	$today = date("Y-m-d H:i:s");
	if ($presence['DOUscita'] > $today) {
		$_SESSION["presence"] = $presence['CodicePresenza'];
	}
}

function endPresence() {
	$_SESSION['presence'] = null;
}

function isPresenceActive() {
	return !empty($_SESSION['presence']);
}

function logOutuser() {
	if (isUserLoggedIn()) {
		$_SESSION["email"] = null;
	}
}

function logInAdmin() {
	$_SESSION["adminLoggedIn"] = true;
}

function adminLogOut() {
	if (isAdminLoggedIn()) {
		$_SESSION["adminLoggedIn"] = null;
	}
}

function isUserLoggedIn() {
    return !empty($_SESSION['email']);
}

function isAdminLoggedIn(){
	return !empty($_SESSION["adminLoggedIn"]);
}

function isAuthorized($email = '') {
	if (!isset($_SESSION['email'])) {
		return isAdminLoggedIn();
	} else {
		return (($_SESSION['email'] == $email ) || isAdminLoggedIn());
	}
}

?>