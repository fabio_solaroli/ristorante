<?php

	require_once("../baseConfiguration.php");

	$casts = ['ingredients' => 'array'];

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if ($uri[3] == 'dish') {
		if (!isset($uri[4])) {
			switch ($requestMethod) {
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['name']) && isset($_POST['ingredients']) && isset($_POST['category']) && isset($_POST['price']) && isset($_POST['preparationTime'])) {
							$result = $dbh->addNewDish(json_decode($_POST['ingredients']), $_POST['category'], $_POST['name'], $_POST['price'], $_POST['preparationTime']);
							if ($result[0]) {
								response(201, "Piatto creata", $result[1]);
							} else {
								response(409, "Piatto già esistente", NULL);
							}
						} else {
							response(400, "Bad Request", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		} else if ($uri[4] == 'category') {
			switch ($requestMethod) {
				case 'GET':
					if (!isset($uri[5])) {
						response(200, "", $dbh->getUserAllDishesByCategory());
					} else if ($uri[5] === 'admin') {
						if(isAuthorized()) {
							response(200, "", $dbh->getAdminAllDishesByCategory());
					 	} else {
							response(401, "Unauthorized", null);
						}
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		} else if ($uri[4] == 'price') {
			switch ($requestMethod) {
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['dishId']) && isset($_POST['price'])) {
							$result = $dbh->editDishPrice($_POST['dishId'], $_POST['price']);
							if ($result) {
								response(200, "Prezzo modificato", null);
							} else {
								response(500, "Prezzo non modificato", NULL);
							}
						} else {
							response(400, "Bad Request (Missing parameters)", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				default:
					response(400, "Bad Request (Only Post)", NULL);
					break;
			}
		} else {

		}
	} else if ($uri[3] == 'category') {
		if (!isset($uri[4])) {
			switch ($requestMethod) {
				case 'GET':
					response(200, "", $dbh->getCategories());
					break;
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['name'])) {
							$result = $dbh->addNewCategory($_POST['name']);
							if ($result[0]) {
								response(201, "Creata categoria", $result[1]);
							} else {
								response(409, "Categoria già esistente", NULL);
							}
						} else {
							response(400, "Bad Request (Missing parameters)", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		} else {

		}
 	} else if ($uri[3] == 'beverage') {
		if (!isset($uri[4])) {
			switch ($requestMethod) {
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['name']) && isset($_POST['quantity']) && isset($_POST['price']) && isset($_POST['capacity'])) {
							$result = $dbh->addNewBeverage($_POST['name'], $_POST['capacity'], $_POST['price'], $_POST['quantity']);
							if ($result[0]) {
								response(201, "Bevanda creata", $result[1]);
							} else {
								response(409, "Bevanda già esistente", NULL);
							}
						} else {
							response(400, "Bad Request", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				case 'GET':
					response(200, "", $dbh->getUserAllBeverages());
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		} else if ($uri[4] == 'price') {
			switch ($requestMethod) {
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['beverageId']) && isset($_POST['price'])) {
							$result = $dbh->editBeveragePrice($_POST['beverageId'], $_POST['price']);
							if ($result) {
								response(200, "Prezzo modificato", null);
							} else {
								response(500, "Prezzo non modificato", NULL);
							}
						} else {
							response(400, "Bad Request", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		} else if ($uri[4] == 'admin') {
			switch ($requestMethod) {
				case 'GET':
					if(isAuthorized()) {
						response(200, "", $dbh->getAdminAllBeverages());
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		}
	} else if ($uri[3] == 'ingredient') {
		if (!isset($uri[4])) {
				switch ($requestMethod) {
					case 'GET':
						switch ($requestMethod) {
							case 'GET':
									response(200, "", $dbh->getIngredients());
								break;
							default:
								response(400, "Bad Request", NULL);
								break;
						}
						break;
					case 'POST':
						if(isAuthorized()) {
							if(isset($_POST['name']) && isset($_POST['quantity'])) {
								$result = $dbh->addNewIngredient($_POST['name'], $_POST['quantity']);
								if ($result[0]) {
									response(201, "Ingrediente creato", $result[1]);
								} else {
									response(409, "Ingrediente già esistente", NULL);
								}
							} else {
								response(400, "Bad Request", NULL);
							}
						} else {
							response(401, "Unauthorized", null);
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
		} else {

		}
	} else if ($uri[3] == 'menu') {
		if (isAuthorized()) {
			if ($uri[4] == 'dish') {
				switch ($requestMethod) {
					case 'DELETE':
						$result = $dbh->removeDishFromMenu($uri[5]);
						if ($result) {
							response(200, "Piatto rimosso con successo", $result);
						} else {
							response(500, "Piatto non rimosso", $result);
						}
						break;
					case 'PUT':
						$result = $dbh->restoreDishInMenu($uri[5]);
						if ($result) {
							response(200, "Piatto reinserito con successo", $result);
						} else {
							response(500, "Piatto non reinserito", $result);
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			} else if ($uri[4] == 'beverage') {
				switch ($requestMethod) {
					case 'DELETE':
						$result = $dbh->removeBeverageFromMenu($uri[5]);
						if ($result) {
							response(200, "Bevanda rimossa con successo", null);
						} else {
							response(500, "Bevanda non rimossa", null);
						}
						break;
					case 'PUT':
						$result = $dbh->restoreBeverageInMenu($uri[5]);
						if ($result) {
							response(200, "Bevanda reinserita con successo", null);
						} else {
							response(500, "Bevanda non reinserita", null);
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			} else {
				response(400, "Bad Request", NULL);
			}
		} else {
			response(401, "Unauthorized", null);
		}
	} else {
		response(400, "Bad Request", NULL);
	}

?>