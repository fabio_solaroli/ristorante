<?php

	require_once("../baseConfiguration.php");

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if (isAuthorized()) {
		if (!isset($uri[3])) {
			switch ($requestMethod) {
				case 'GET':
					response(200, "", $dbh->checkWarehouse());
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		}	else if ($uri[3] == 'ingredient') {
			if ($uri[4] == 'progress') {
				switch ($requestMethod) {
					case 'GET':
						response(200, "", $dbh->showIngredientWarehouseProgress($uri[5]));
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			} else if ($uri[4] = 'transaction') {
				switch ($requestMethod) {
					case 'POST':
						if (isset($_POST["ingredientId"]) && isset($_POST["quantity"])) {
							$response = $dbh->createIngredientTransaction($_POST["ingredientId"], $_POST["quantity"]);
							if ($response) {
								response(200, "Transazione creata", null);
							} else {
								response(500, "Quantità negativa", null);
							}
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			} else {
				switch ($requestMethod) {
					case 'GET':
						$result = $dbh->getIngredientInformation($uri[4]);
						if (!$result[0]) {
							response(404, "Ingredient Not Found", NULL);
						} else {
							response(200, "Ingredient Found", $result[1]);
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			}
		} else if ($uri[3] == 'beverage') {
			if ($uri[4] == 'progress') {
				switch ($requestMethod) {
					case 'GET':
						response(200, "", $dbh->showBeverageWarehouseProgress($uri[5]));
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			} else if ($uri[4] = 'transaction') {
				switch ($requestMethod) {
					case 'POST':
						if (isset($_POST["beverageId"]) && isset($_POST["quantity"])) {
							$response = $dbh->createBeverageTransaction($_POST["beverageId"], $_POST["quantity"]);
							if ($response) {
								response(200, "Transazione creata", null);
							} else {
								response(500, "Quantità negativa", null);
							}
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			} else {
				switch ($requestMethod) {
					case 'GET':
						$result = $dbh->getBeverageInformation($uri[4]);
						if (!$result[0]) {
							response(404, "Beverage Not Found", NULL);
						} else {
							response(200, "Beverage Found", $result[1]);
						}
						break;
					default:
						response(400, "Bad Request", NULL);
						break;
				}
			}
		} else {
			response(400, "Bad Request", NULL);
		}
	} else {
		response(401, "Unauthorized", null);
	}

?>