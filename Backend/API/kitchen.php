<?php

	require_once("../baseConfiguration.php");

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if (isAuthorized()) {
		if (!isset($uri[3])) {
			switch ($requestMethod) {
				case 'GET':
					response(200, "", $dbh->getDishesNotReady()); 
					break;
				case 'POST':
					if (isset($_POST["CodicePresenza"]) && isset($_POST["NumeroPortata"]) && isset($_POST["CodicePiatto"])) {
						$result = $dbh->setDishReady($_POST["CodicePresenza"], $_POST["NumeroPortata"], $_POST["CodicePiatto"]);
						if ($result) {
							response(200, "Piatto pronto", null);
						} else {
							response(500, "Piatto non pronto", null);
						}
					} else {
						response(400, "Missing Parameters", NULL);
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		}
	} else {
		response(401, "Unauthorized", null);
	}
?>