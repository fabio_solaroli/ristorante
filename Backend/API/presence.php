<?php

	require_once("../baseConfiguration.php");

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if (!isset($uri[3])) {
		switch ($requestMethod) {
			case 'POST':
				if (isset($_POST["table"]) && isset($_POST["nPeople"])) {
					$result = $dbh->createNewPresence($_POST["nPeople"], $_POST["table"]);
					if ($result[0]) {
						response(201, "Presenza creato", $result[1]);
					} else {
						response(500, "Presenza non creata", null);
					}
				} else {
					response(400, "Missing Parameters", NULL);
				}
				break;
			case 'GET':
				if (!isPresenceActive()) {
					response(200, "Presence not active", null);
				} else {
					$result = $dbh->getPresence($_SESSION['presence']);
					$today = date("Y-m-d H:i:s");
					if ($result[1]['DOUscita'] < $today) {
						$_SESSION["presence"] = null;
					} else {
						if ($result[1]["EmailUtente"] != null) {
							if (isAuthorized($result[1]["EmailUtente"])) {
								response(200, "Presence active", $result[1]);
							} else {
								response(401, "Unauthorized", null);
							}
						} else {
							response(200, "Presence active", $result[1]);
						}
					}
				}
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	} else if ($uri[3] == 'all') {
		switch ($requestMethod) {
			case 'GET':
				if (isAuthorized()) {
					response(200, "", $dbh->getAllPresences());
				} else {
					response(401, "Unauthorized", null);
				}
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	} else if ($uri[3] == 'active') {
		switch ($requestMethod) {
			case 'GET':
				if (isAuthorized()) {
					response(200, "", $dbh->getAllActivePresences());
				} else {
					response(401, "Unauthorized", null);
				}
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	} else if($uri[3] == 'user') {
		switch ($requestMethod) {
			case 'GET':
				if (isAuthorized($uri[4])) {
					if ($uri[5] == 'all') {
						response(200, "", $dbh->getAllUserPresences($uri[4]));
					} else if ($uri[5] == 'active') {
						$result = $dbh->getUserActivePresence($uri[4]);
						if ($result[0]) {
							response(200, "Presence found", $result[1]);
						} else {
							response(404, "Presence not found", null);
						}
					} else {
						response(400, "Bad Request", NULL);
					}
				} else {
					response(401, "Unauthorized", NULL);
				}
				break;
			case 'POST':
				if (isset($_POST['email']) && isset($_POST['presenceId'])) {
					$result = $dbh->getPresence($_POST['presenceId']);
					if ($result[0]['EmailUtente'] == null) {
						$response = $dbh->addUserToPresence($_POST['email'], $_POST['presenceId']);
						if ($response) {
							respose(200, "User added to presence", null);
						} else {
							respose(500, "", null);
						}
					} else {
						response(409, "Presence has already a user", null);
					}
				}
			default:
				response(400, "Bad Request", NULL);
				break;
		}
		
	} else if ($uri[3] == 'details') {
		switch ($requestMethod) {
			case 'GET':
				$result = $dbh->getPresenceRecap($uri[4]);
				if (isAuthorized($result[0]['EmailUtente'])) {
					response(200, "", $result);
				} else {
					response(401, "Unauthorized", NULL);
				}
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	} else if ($uri[3] == 'course') {
		switch ($requestMethod) {
			case 'POST':
				if (isset($_POST['dishes']) && isset($_POST['beverages'])) {
					if (isPresenceActive()) {
						$result = $dbh->getPresence($_SESSION["presence"]);
						if ($result[0]) {
							if ($result[1]["EmailUtente"] == null || isAuthorized($result[1]["EmailUtente"])) {
								$result = $dbh->orderCourse(json_decode($_POST['dishes']), json_decode($_POST['beverages']), $_SESSION['presence']);
								response(201, "Portata creata con successo", $result);
							} else {
								response(401, "Unauthorized", NULL);
							}
						} else {
							$result = $dbh->orderCourse(json_decode($_POST['dishes']), json_decode($_POST['beverages']), $_SESSION['presence']);
							response(201, "Portata creata con successo", $result);
						}
					} else {
						response(401, "Unauthorized", NULL);
					}
				} else {
					response(400, "Bad Request", NULL);
				}
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	} else {
		switch ($requestMethod) {
			case 'GET':
				$result = $dbh->getPresence($uri[3]);
				if (!$result[0]) {
					response(404, "Presence not found", null);
				} else {
					$userEmail = $result[1]["EmailUtente"];
					if ($userEmail != null) {
						if (isAuthorized($userEmail)) {
							startPresence($result[1]);
							response(200, "HasUser", $result[1]);
						} else {
							response(401, "Questa presenza appartiene ad un altro utente", null);
						}
					} else {
						startPresence($result[1]);
						response(200, "Presence found", $result[1]);
					}
				}
				break;
			case 'PUT':
				if (isUserLoggedIn()) {
					$dbh->addUserToPresence($_SESSION["email"], $uri[3]);
				}
			case 'DELETE':
				$result = $dbh->endPresence($uri[3]);
				endPresence();
				if ($result) {
					response(200, "Presenza conclusa", true);
				} else {
					response(500, "Something went wrong", false);
				}
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	}
?>