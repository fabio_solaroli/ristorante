<?php

	require_once("../baseConfiguration.php");

	$adminUsername = 'admin';
	$adminPassword = 'password';

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if (!isset($uri[3])) {
		switch ($requestMethod) {
			case 'GET':
				if (isAdminLoggedIn()) {
					response(200, "Admin Logged In", true);
				} else {
					response(200, "Admin Not Logged In", false);
				}
				break;
			case 'POST':
				if (isset($_POST["username"]) && isset($_POST["password"])) {
					if (!(strcmp($_POST["username"], $adminUsername) == 0) || !(strcmp($_POST["password"], $adminPassword) == 0)) {
						response(400, "Email o password errate", false); 
					} else {
						logInAdmin();
						response(200, "Log In effettuato con successo", true);
					}
				}
				break;
			case 'DELETE':
				adminLogOut();
				response(200, "Log Out effettuato con successo", true);
				break;
			default:
				response(400, "Bad Request", NULL);
				break;
		}
	}	else  {
		response(400, "Bad Request", NULL);
	}
?>