<?php

	require_once("../baseConfiguration.php");

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if (!isset($uri[3])) {
		switch ($requestMethod) {
			case 'GET':
				if (isUserLoggedIn()) {
					response(200, "User Logged In", $_SESSION['email']);
				} else {
					response(200, "User Not Logged In", null);
				}
				break;
			case 'POST':
				if (isset($_POST["email"]) && isset($_POST["password"])) {
					$result = $dbh->checkUserLogin($_POST["email"], $_POST["password"]);
					if (!$result[0]) {
						response(400, "Email o password errate", null); 
					} else {
						logInUser($_POST["email"]);
						if (isset($_SESSION['presence'])) {
							$presence = $dbh->getPresence($_SESSION['presence']);
							if ($presence[1]['EmailUtente'] == null) {
								$dbh->addUserToPresence($_POST['email'], $_SESSION['presence']);
							}
						} else {
							$result = $dbh->getUserActivePresence($_POST['email']);
							if ($result[0]) {
								$_SESSION['presence'] = $result[1]["CodicePresenza"];
							}
						}
						response(200, "Log In effettuato con successo", $result[1]);
					}
				}
				break;
			case 'DELETE':
				logOutUser();
				response(200, "Log Out effettuato con successo", true);
				break;
			default;
				response(400, "Bad Request", NULL);
				break;
		}
	} else if($uri[3] == 'new') {
		switch ($requestMethod) {
			case 'POST':
				if (isset($_POST["email"]) && isset($_POST["password"]) && isset($_POST["name"]) && isset($_POST["surname"])) {
					$result = $dbh->registerNewUser($_POST["email"], $_POST["password"], $_POST["name"], $_POST["surname"]);
					if ($result[0]) {
						logInUser($result[1]);
						response(201, "Utente creato", $result[1]);
					} else {
						response(409, "Mail già utilizzata per un altro account", NULL);
					}
				} else {
					response(400, "Missing Parameters", NULL);
				}
				break;
			default;
				response(400, "Bad Request", NULL);
				break;
		}
	} else {
		switch ($requestMethod) {
			case 'GET':
				if(isAuthorized($uri[3])) {
					$result = $dbh->getUserInformation($uri[3]);
					if ($result[0]) {
						response(200, "User found", $result[1]);
					} else {
						response(404, "User not found", null);
					}
				} else {
					response(401, "Unauthorized", null);
				}
				break;
			default;
				response(400, "Bad Request", NULL);
				break;
		}
	}
?>