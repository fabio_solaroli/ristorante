<?php

	require_once("../baseConfiguration.php");

	$requestMethod = $_SERVER["REQUEST_METHOD"];

	$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$uri = explode('/', $uri);

	if (!isset($uri[3])) {

	} else if($uri[3] == 'table') {
		if (!isset($uri[4])) {
			switch ($requestMethod) {
				case 'GET':
					if (isAuthorized()) {
						response(200, "", $dbh->getTables());
					} else {
						response(401, "Unauthorized", NULL);
					}
					break;
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['name']) && isset($_POST['maxPeople'])) {
							$result = $dbh->addNewTable($_POST['name'], $_POST['maxPeople']);
							if ($result[0]) {
								response(201, "Tavolo creato", $result[1]);
							} else {
								response(409, "Tavolo già esistente", NULL);
							}
						} else {
							response(400, "Bad Request", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		} else {
			switch ($requestMethod) {
				case 'GET':
					if (isAuthorized()) {
						$result = $dbh->getTable($uri[4]);
						if ($result[0]) {
							response(200, "Table Found", $result[1]);
						} else {
							response(404, "Table Not Found", null);
						}
					} else {
						response(401, "Unauthorized", NULL);
					}
					break;
				case 'POST':
					if(isAuthorized()) {
						if(isset($_POST['maxPeople'])) {
							$result = $dbh->changeTableMaxPeople($uri[4], $_POST['maxPeople']);
							if ($result) {
								response(200, "Persone massime modificate", null);
							} else {
								response(500, "Persone massime non modificate", NULL);
							}
						} else {
							response(400, "Bad Request", NULL);
						}
					} else {
						response(401, "Unauthorized", null);
					}
					break;
				case 'DELETE':
					$result = $dbh->removeTableFromRestaurant($uri[4]);
					if ($result) {
						response(200, "Tavolo rimosso con successo", $result);
					} else {
						response(500, "Tavolo non rimosso", $result);
					}
					break;
				case 'PUT':
					$result = $dbh->restoreTableInRestaurant($uri[4]);
					if ($result) {
						response(200, "Tavolo reinserito con successo", $result);
					} else {
						response(500, "Tavolo non reinserito", $result);
					}
					break;
				default:
					response(400, "Bad Request", NULL);
					break;
			}
		}
	}

?>