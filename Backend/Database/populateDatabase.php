<?php

	require_once("./baseConfiguration.php");

	if (empty($dbh->getAllPresences())) {
		$user1 = $dbh->registerNewUser("test@test.com", "password", "Test", "Test");
		$user2 = $dbh->registerNewUser("ciccio@test.com", "password", "Ciccio", "Pasticcio");
	
		$ing1 = $dbh->addNewIngredient("Pomodoro", 200)[1];
		$ing2 = $dbh->addNewIngredient("Pasta", 200)[1];
		$ing3 = $dbh->addNewIngredient("Lattuga", 200)[1];
		$ing4 = $dbh->addNewIngredient("Mozzarella", 200)[1];
		$ing5 = $dbh->addNewIngredient("Tonno", 200)[1];
		$ing6 = $dbh->addNewIngredient("Fragole", 200)[1];
		$ing7 = $dbh->addNewIngredient("Cioccolato", 200)[1];
		$cat1 = $dbh->addNewCategory("Primi")[1];
		$cat2 = $dbh->addNewCategory("Secondi")[1];
		$cat3 = $dbh->addNewCategory("Dolci")[1];
	
		$dish1 = $dbh->addNewDish(array((object)array("id" => $ing1, "quantity" => 20), (object)array("id" => $ing2, "quantity" => 100)), $cat1, "Pasta al pomodoro", 20, 10)[1];
		$dish2 = $dbh->addNewDish(array((object)array("id" => $ing1, "quantity" => 20), (object)array("id" => $ing3, "quantity" => 100), (object)array("id" => $ing4, "quantity" => 20), (object)array("id" => $ing5, "quantity" => 20)), $cat2, "Insalata con tonno e mozzarelle", 15, 5)[1];
		$dish3 = $dbh->addNewDish(array((object)array("id" => $ing6, "quantity" => 10), (object)array("id" => $ing7, "quantity" => 5)), $cat3, "Fragole e cioccolato", 5, 2)[1];
		$beverage1 = $dbh->addNewBeverage("Acqua naturale", 1000, 2, 500)[1];
		$beverage2 = $dbh->addNewBeverage("Acqua frizzante", 1000, 5, 500)[1];
	
		$table1 = $dbh->addNewTable("1", 2)[1];
		$table2 = $dbh->addNewTable("2", 4)[1];
		$table3 = $dbh->addNewTable("3", 2)[1];
	
		sleep(1);
	
		$presence1 = $dbh->createNewPresence(1, $table1)[1]["CodicePresenza"];
		$dbh->addUserToPresence("ciccio@test.com", $presence1);
		
		$dbh->orderCourse(array((object)array("CodicePiatto" => $dish1, "Quantita" => 1), (object)array("CodicePiatto" => $dish2, "Quantita" => 2)), array((object)array("CodiceBevanda" => $beverage1, "Quantita" => 1)), $presence1);
		
		sleep(1);
		
		$dbh->orderCourse(array((object)array("CodicePiatto" => $dish2, "Quantita" => 1)), array(), $presence1);
	
		sleep(1);
	
		$dbh->setDishReady($presence1, 1, $dish1);
		$dbh->setDishReady($presence1, 1, $dish2);
		$dbh->setDishReady($presence1, 2, $dish2);
		
		sleep(1);
	
		$dbh->orderCourse(array((object)array("CodicePiatto" => $dish3, "Quantita" => 1)), array(), $presence1);
	
		sleep(1);
	
		$dbh->setDishReady($presence1, 3, $dish3);
	
		sleep(1);
		
		$dbh->endPresence($presence1);
		
		sleep(1);
	
		$dbh->editDishPrice($dish1, 25);
		$dbh->editBeveragePrice($beverage2, 3);
	
		sleep(1);
	
		$presence2 = $dbh->createNewPresence(4, $table2)[1]["CodicePresenza"];
	
		$dbh->addUserToPresence("test@test.com", $presence2);
	
		$dbh->orderCourse(array((object)array("CodicePiatto" => $dish1, "Quantita" => 2)), array((object)array("CodiceBevanda" => $beverage1, "Quantita" => 2)), $presence2);
	}
?>