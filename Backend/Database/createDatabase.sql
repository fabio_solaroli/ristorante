create schema if not exists basiDiDati default character set utf8;
use basiDiDati;

create table if not exists Utente (
	Email				varchar(50) not null,
	Passphrase	char(60) 		not null,
	Nome 				varchar(30) not null,
	Cognome 		varchar(30) not null,

	constraint PK_Utente primary key (Email)
);

create table if not exists Tavolo (
	CodiceTavolo	int 				not null auto_increment,
	Nome					varchar(50)	not null unique,
	MaxPosti			int					not null,
	InRistorante	boolean			not null,
	PostiOccupati	int					not null,

	constraint PK_Tavolo primary key (CodiceTavolo)					
);

create table if not exists Presenza (
	CodicePresenza	int 					not null auto_increment,
	EmailUtente			varchar(50),
	NumeroPersone 	int 					not null,
	CodiceTavolo 		int 					not null,
	Spesa						decimal(6, 2) not null,
	DOArrivo				datetime			not null,
	DOUscita				datetime			not null,

	constraint PK_Presenza primary key (CodicePresenza),
	foreign key (EmailUtente) references Utente (Email),
	foreign key (CodiceTavolo) references Tavolo (CodiceTavolo)
);

create table if not exists Portata (
	NumeroPortata		int 			not null,
	CodicePresenza 	int 			not null,
	Orario 					datetime 	not null,

	constraint PK_Portata primary key (NumeroPortata, CodicePresenza),
	foreign key (CodicePresenza) references Presenza (CodicePresenza)
);

create table if not exists Categoria (
	CodiceCategoria int 				not null auto_increment,
	Nome 						varchar(60) not null unique,

	constraint PK_Categoria primary key (CodiceCategoria)
);

create table if not exists Piatto (
	CodicePiatto 			int 					not null auto_increment,
	CodiceCategoria 	int 					not null,
	Nome 							varchar(60) 	not null unique,
	InMenu					 	boolean 			not null,
	PrezzoAttuale 		decimal(6, 2) not null,
	TempoPreparazione int 					not null,

	constraint PK_Piatto primary key (CodicePiatto),
	foreign key (CodiceCategoria) references Categoria (CodiceCategoria)
);

create table if not exists PiattoInPortata (
	NumeroPortata 	int 			not null,
	CodicePresenza 	int 			not null,
	CodicePiatto 		int 			not null,
	Pronto					datetime	not null,
	Quantita 				int 			not null,

	constraint PK_PiattoInPortata primary key (NumeroPortata, CodicePresenza, CodicePiatto),
	foreign key (NumeroPortata) references Portata (NumeroPortata),
	foreign key (CodicePresenza) references Portata (CodicePresenza),
	foreign key (CodicePiatto) references Piatto (CodicePiatto),
	index PiattoInPortataPronto (Pronto)
);

create table if not exists Bevanda (
	CodiceBevanda 			int 					not null auto_increment,
	Nome 								varchar(60) 	not null,
	Capienza						int						not null,
	InMenu					 		boolean 			not null,
	PrezzoAttuale 			decimal(6, 2) not null,
	QuantitaInMagazzino int 					not null,

	constraint PK_Bevanda primary key (CodiceBevanda),
	constraint UC_Bevanda unique (Nome, Capienza)
);

create table if not exists BevandaInPortata (
	NumeroPortata 	int not null,
	CodicePresenza 	int not null,
	CodiceBevanda 	int not null,
	Quantita 				int not null,

	constraint PK_PiattoInPortata primary key (NumeroPortata, CodicePresenza, CodiceBevanda),
	foreign key (NumeroPortata) references Portata (NumeroPortata),
	foreign key (CodicePresenza) references Portata (CodicePresenza),
	foreign key (CodiceBevanda) references Bevanda (CodiceBevanda)
);

create table if not exists Prezzo (
	CodicePrezzo 	int 					not null auto_increment,
	CodiceBevanda int,
	CodicePiatto 	int,
	DaData 				datetime 			not null,
	AData 				datetime 			not null,
	Prezzo 				decimal(6, 2) not null,

	constraint PK_Prezzo primary key (CodicePrezzo),
	foreign key (CodiceBevanda) references Bevanda (CodiceBevanda),
	foreign key (CodicePiatto) references Piatto (CodicePiatto)
);

create table if not exists Ingrediente (
	CodiceIngrediente 	int 				not null auto_increment,
	Nome 								varchar(60) not null unique,
	QuantitaInMagazzino int 				not null,

	constraint PK_Ingrediente primary key (CodiceIngrediente)
);

create table if not exists IngredienteInPiatto (
	CodicePiatto 			int not null,
	CodiceIngrediente int not null,
	Quantita 					int not null,

	constraint PK_IngredienteInPiatto primary key (CodicePiatto, CodiceIngrediente),
	foreign key (CodiceIngrediente) references Ingrediente (CodiceIngrediente),
	foreign key (CodicePiatto) references Piatto (CodicePiatto)
);

create table if not exists Transazione (
	CodiceTransazione int 			not null auto_increment,
	Quantita 					int 			not null,
	DataOra						datetime 	not null,
	CodiceBevanda 		int,
	CodiceIngrediente int,

	constraint PK_Transazione primary key (CodiceTransazione),
	foreign key (CodiceIngrediente) references Ingrediente (CodiceIngrediente),
	foreign key (CodiceBevanda) references Bevanda (CodiceBevanda)
);