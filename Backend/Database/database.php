<?php

class DatabaseHelper{
	private $db;
	private $DistantPast = '1000-01-01 00:00:00';
	private $DistantFuture = '4000-01-01 00:00:00';

	private $costoCoperto = 1.00;

	public function __construct($servername, $username, $password, $dbname, $port){
		$this->db = new mysqli($servername, $username, $password, $dbname, $port);
			if ($this->db->connect_error) {
					die("Connection failed: " . $this->db->connect_error);
			}
	}

	//----------- 1 ----------//

	public function registerNewUser($email, $password, $name, $surname) {
		// check if already exits a user with that email
		$stmt = $this->db->prepare ("SELECT Email FROM Utente WHERE Email = ? LIMIT 1");
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			// insert new user
			$passwordHash = password_hash($password, PASSWORD_DEFAULT);
			$stmt = $this->db->prepare ("	INSERT INTO Utente (Email, Passphrase, Nome, Cognome)
																		values	(?, ?, ?, ?)");
			$stmt->bind_param('ssss', $email, $passwordHash, $name, $surname);
			$stmt->execute();

			return array(true, $email);
		} else {
			return array(false, null);
		}
	}

	//----------- 2 ----------//

	public function addNewDish($ingredients, $categoryCode, $name, $price, $preparationTime) {
		// check if already exists a dish with that name
		$stmt = $this->db->prepare ("SELECT Nome FROM Piatto WHERE Nome = ? LIMIT 1");
		$stmt->bind_param('s',$name);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			// insert new dish
			$stmt = $this->db->prepare ("	INSERT INTO Piatto (CodiceCategoria, Nome, InMenu, PrezzoAttuale, TempoPreparazione)
																		values	(?, ?, true, ?, ?)");
			$stmt->bind_param('isdi', $categoryCode, $name, $price, $preparationTime);
			$stmt->execute();

			// get the id of the dish
			$stmt = $this->db->prepare ("SELECT LAST_INSERT_ID() AS DISH_ID");
			$stmt->execute();
			$result = $stmt->get_result();
			$dishId = $result->fetch_all(MYSQLI_ASSOC)[0]["DISH_ID"];

			// insert all the ingredients in the dish with their quantity
			foreach ($ingredients as $ingredient) {
				$stmt = $this->db->prepare ("	INSERT INTO IngredienteInPiatto (CodicePiatto, CodiceIngrediente, Quantita)
																			values	(?, ?, ?)");
				$stmt->bind_param('iii', $dishId, $ingredient->id, $ingredient->quantity);
				$stmt->execute();
			}

			// insert the price of the dish
			$stmt = $this->db->prepare ("	INSERT INTO Prezzo (CodicePiatto, DaData, AData, Prezzo)
																		values	(LAST_INSERT_ID(), NOW(), ?, ?)");
			$stmt->bind_param('sd', $this->DistantFuture, $price);
			$stmt->execute();
			return array(true, $dishId);
		} else {
			return array(false, null);
		}
	}

	//----------- 3 ----------//

	public function createNewPresence($numberOfPeople, $tableId) {
		// insert new presence
		$coverCost = $numberOfPeople * $this->costoCoperto;
		$stmt = $this->db->prepare ("	INSERT INTO Presenza (NumeroPersone, CodiceTavolo, Spesa, DOArrivo, DOUscita)
																	values	(?, ?, ?, NOW(), ?)");
		$stmt->bind_param('iids', $numberOfPeople, $tableId, $coverCost, $this->DistantFuture);
		$success = $stmt->execute();

		// return presence
		$stmt = $this->db->prepare ("	SELECT * FROM Presenza
																	WHERE CodicePresenza = LAST_INSERT_ID()");
		$stmt->execute();
		$result = $stmt->get_result();
		$presence = $result->fetch_all(MYSQLI_ASSOC)[0];

		$stmt = $this->db->prepare ("	UPDATE Tavolo
																	SET PostiOccupati = ?
																	WHERE CodiceTavolo = ?");
		$stmt->bind_param('ii', $numberOfPeople, $tableId);
		$stmt->execute();

		return array($success, $presence);
	}

	public function addUserToPresence($email, $presenceId) {
		// check if user il already in a presence
		$stmt = $this->db->prepare ("	SELECT U.Email FROM Utente AS U 
																	JOIN Presenza AS P ON U.Email = P.EmailUtente
																	WHERE U.Email = ?
																	AND P.DOUscita > NOW()
																	AND P.DOArrivo <= NOW()
																	LIMIT 1");
		$stmt->bind_param('s', $email);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);

		if (empty($result)) {
			// insert new user
			$stmt = $this->db->prepare ("	UPDATE Presenza
																		SET EmailUtente = ?
																		WHERE CodicePresenza = ?");
			$stmt->bind_param('si', $email, $presenceId);
			return $stmt->execute();
		} else {
			return false;
		}
	}

	public function endPresence($presenceId) {
		$stmt = $this->db->prepare ("	UPDATE Presenza
																	SET DOUscita = NOW()
																	WHERE CodicePresenza = ?
																	AND DOArrivo <= NOW()
																	AND DOUscita > NOW()");
		$stmt->bind_param('i', $presenceId);
		$result1 = $stmt->execute();
		
		$stmt = $this->db->prepare ("	UPDATE Tavolo
																	SET PostiOccupati = PostiOccupati - (SELECT NumeroPersone FROM Presenza
																											 								 WHERE CodicePresenza = ?)
																	WHERE CodiceTavolo = (SELECT CodiceTavolo FROM Presenza
																												WHERE CodicePresenza = ?)");
		$stmt->bind_param('ii', $presenceId, $presenceId);
		$result2 = $stmt->execute();

		return ($result1 && $result2);
	}

	//----------- 4 ----------//

	public function checkBeveragesQuantity() {
		$stmt = $this->db->prepare ("	SELECT Nome, CodiceBevanda, QuantitaInMagazzino FROM Bevanda");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function checkIngredientsQuantity() {
		$stmt = $this->db->prepare ("	SELECT Nome, CodiceIngrediente, QuantitaInMagazzino FROM Ingrediente");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function checkWarehouse() {
		return array(
			"Beverages" => $this->checkBeveragesQuantity(),
			"Ingredients" => $this->checkIngredientsQuantity()
		);
	}

	//----------- 5 ----------//

	public function showUserPresences($email) {
		$stmt = $this->db->prepare ("	SELECT NumeroPersone, Spesa, DOArrivo, DOUscita FROM Presenza
																	WHERE EmailUtente = ?");
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	//----------- 6 ----------//

	public function getPresenceRecap($presenceId) {
		$stmt = $this->db->prepare ("	SELECT  Presenza.*,
																					CONCAT(Utente.Nome, ' ', Utente.Cognome) AS NomeUtente,
																					Tavolo.Nome AS NomeTavolo,
																					Portata.NumeroPortata, Portata.Orario,
																					InPortata.Codice, InPortata.Pronto, InPortata.Nome, InPortata.Prezzo, InPortata.Quantita, InPortata.Tipo
																					FROM    Presenza
																					LEFT OUTER JOIN 	Utente	ON (Utente.Email = Presenza.EmailUtente)
																					JOIN							Tavolo 	ON (Tavolo.CodiceTavolo = Presenza.CodiceTavolo)
																					LEFT OUTER JOIN    					Portata	ON (Portata.CodicePresenza = Presenza.CodicePresenza)
																					LEFT OUTER JOIN    (
																																	SELECT  'BEVANDA'       AS Tipo,
																																																	BevandaInPortata.CodiceBevanda   	AS Codice,
																																																	BevandaInPortata.CodicePresenza  	AS CodicePresenza,
																																																	BevandaInPortata.NumeroPortata   	AS NumeroPortata,
																																																	BevandaInPortata.Quantita 			 	AS Quantita,
																																																	NULL 															AS Pronto,
																																																	Bevanda.Nome            					AS Nome,
																																																	Prezzo.Prezzo           					AS Prezzo,
																																																	Prezzo.DaData           					AS DaData,
																																																	Prezzo.AData            					AS AData
																																	FROM    BevandaInPortata
																																	JOIN    Bevanda         ON (Bevanda.CodiceBevanda = BevandaInPortata.CodiceBevanda)
																																	JOIN    Prezzo          ON (Prezzo.CodiceBevanda = Bevanda.CodiceBevanda)
																																	UNION ALL
																																	SELECT  'PIATTO'        AS Tipo,
																																																	PiattoInPortata.CodicePiatto    AS Codice,
																																																	PiattoInPortata.CodicePresenza  AS CodicePresenza,
																																																	PiattoInPortata.NumeroPortata   AS NumeroPortata,
																																																	PiattoInPortata.Quantita 			 	AS Quantita,
																																																	PiattoInPortata.Pronto					AS Pronto,
																																																	Piatto.Nome             				AS Nome,
																																																	Prezzo.Prezzo           				AS Prezzo,
																																																	Prezzo.DaData           				AS DaData,
																																																	Prezzo.AData            				AS AData
																																	FROM    PiattoInPortata
																																	JOIN Piatto ON (Piatto.CodicePiatto = PiattoInPortata.CodicePiatto)
																																	JOIN Prezzo ON (Prezzo.CodicePiatto = Piatto.CodicePiatto)
																					) InPortata     ON (
																																	Portata.NumeroPortata = InPortata.numeroPortata
																																	AND
																																	Presenza.CodicePresenza = InPortata.CodicePresenza
																					)
																					WHERE    Presenza.CodicePresenza = ?
																					AND   ((Portata.NumeroPortata IS NULL) OR (InPortata.DaData < Portata.Orario AND InPortata.AData > Portata.Orario))
																					ORDER BY Portata.NumeroPortata, InPortata.Tipo, InPortata.Nome");
		$stmt->bind_param('i',$presenceId);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	//----------- 7 ----------//

	public function editDishPrice($dishId, $price) {
		$stmt = $this->db->prepare ("	UPDATE Prezzo
																	SET AData = NOW()
																	WHERE CodicePiatto = ?
																	AND DaData <= NOW()
																	AND AData > NOW()");
		$stmt->bind_param('i', $dishId);
		$stmt->execute();

		$stmt = $this->db->prepare ("	INSERT INTO Prezzo (CodicePiatto, DaData, AData, Prezzo)
																	values	(?, NOW(), ?, ?)");
		$stmt->bind_param('isd', $dishId, $this->DistantFuture, $price);
		$stmt->execute();

		$stmt = $this->db->prepare ("	UPDATE Piatto
																	SET PrezzoAttuale = ?
																	WHERE CodicePiatto = ?");
		$stmt->bind_param('di', $price, $dishId);
		return $stmt->execute();		
	}

	public function editBeveragePrice($beverageId, $price) {
		$stmt = $this->db->prepare ("	UPDATE Prezzo
																	SET AData = NOW()
																	WHERE CodiceBevanda = ?
																	AND DaData <= NOW()
																	AND AData > NOW()");
		$stmt->bind_param('i', $beverageId);
		$stmt->execute();

		$stmt = $this->db->prepare ("	UPDATE Bevanda
																	SET PrezzoAttuale = ?
																	WHERE CodiceBevanda = ?");
		$stmt->bind_param('di', $price, $beverageId);
		$stmt->execute();

		$stmt = $this->db->prepare ("	INSERT INTO Prezzo (CodiceBevanda, DaData, AData, Prezzo)
																	values	(?, NOW(), ?, ?)");
		$stmt->bind_param('isd', $beverageId, $this->DistantFuture, $price);
		return $stmt->execute();
	}

	//----------- 8 ----------//

	public function checkIngredientTransactionAvaiability($ingredientId, $quantity) {
		$stmt = $this->db->prepare ("	SELECT * FROM Ingrediente
																	WHERE CodiceIngrediente = ?
																	AND QuantitaInMagazzino + ? >= 0");
		$stmt->bind_param('ii', $ingredientId, $quantity);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function createBeverageTransaction($beverageId, $quantity) {
		$stmt = $this->db->prepare ("	SELECT * FROM Bevanda
																	WHERE CodiceBevanda = ?
																	AND QuantitaInMagazzino + ? >= 0");
		$stmt->bind_param('ii', $beverageId, $quantity);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);

		if (!empty($result)) {
			$stmt = $this->db->prepare ("	INSERT INTO Transazione (Quantita, CodiceBevanda, DataOra)
																		values	(?, ?, NOW())");
			$stmt->bind_param('ii', $quantity, $beverageId);
			$stmt->execute();
			
			$stmt = $this->db->prepare ("	UPDATE Bevanda
																		SET QuantitaInMagazzino = QuantitaInMagazzino + (?)
																		WHERE CodiceBevanda = ?");
			$stmt->bind_param('ii', $quantity, $beverageId);
			return $stmt->execute();
		} else {
			return false;
		}
	}

	public function createIngredientTransaction($ingredientId, $quantity) {
		$result = $this->checkIngredientTransactionAvaiability($ingredientId, $quantity);
		if (!empty($result)) {
			$stmt = $this->db->prepare ("	INSERT INTO Transazione (Quantita, CodiceIngrediente, DataOra)
																		values	(?, ?, NOW())");
			$stmt->bind_param('ii', $quantity, $ingredientId);
			$stmt->execute();
			
			$stmt = $this->db->prepare ("	UPDATE Ingrediente
																		SET QuantitaInMagazzino = QuantitaInMagazzino + (?)
																		WHERE CodiceIngrediente = ?");
			$stmt->bind_param('ii', $quantity, $ingredientId);
			return $stmt->execute();
		} else {
			return false;
		}
	}

	public function createDishTransaction($dishId, $quantity) {
		$stmt = $this->db->prepare ("	SELECT CodiceIngrediente, Quantita FROM IngredienteInPiatto
		WHERE CodicePiatto = ?");
		$stmt->bind_param('i',$dishId);
		$stmt->execute();
		$result = $stmt->get_result();
		$ingredients = $result->fetch_all(MYSQLI_ASSOC);

		foreach ($ingredients as $ingredient) {
			$result = $this->checkIngredientTransactionAvaiability($ingredient["CodiceIngrediente"], $quantity);
			if (empty($result)) {
				return false;
			}
		}

		foreach ($ingredients as $ingredient) {
			$this->createIngredientTransaction($ingredient["CodiceIngrediente"], -$ingredient["Quantita"] * $quantity);
		}
		return true;
	}

	//----------- 9 ----------//

	public function getUserAllDishesOfCategory($categoryId) {
		$stmt = $this->db->prepare ("	SELECT P.*, IIP.*, I.Nome AS NomeIngrediente FROM Piatto AS P
																	JOIN Categoria AS C ON C.CodiceCategoria = P.CodiceCategoria
																	JOIN IngredienteInPiatto AS IIP ON IIP.CodicePiatto = P.CodicePiatto
																	JOIN Ingrediente AS I ON I.CodiceIngrediente = IIP.CodiceIngrediente
																	WHERE C.CodiceCategoria = ?
																	AND IIP.Quantita > I.QuantitaInMagazzino
																	AND P.InMenu = true");
		$stmt->bind_param('i',$categoryId);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getAdminAllDishesOfCategory($categoryId) {
		$stmt = $this->db->prepare ("	SELECT P.*, IIP.*, I.Nome AS NomeIngrediente FROM Piatto AS P
																	JOIN Categoria AS C ON C.CodiceCategoria = P.CodiceCategoria
																	JOIN IngredienteInPiatto AS IIP ON IIP.CodicePiatto = P.CodicePiatto
																	JOIN Ingrediente AS I ON I.CodiceIngrediente = IIP.CodiceIngrediente
																	WHERE C.CodiceCategoria = ?");
		$stmt->bind_param('i',$categoryId);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getUserAllDishesByCategory() {
		$stmt = $this->db->prepare ("	SELECT P.*, C.Nome AS NomeCategoria, IIP.*, I.Nome AS NomeIngrediente, I.QuantitaInMagazzino FROM Categoria AS C
																	JOIN Piatto AS P ON P.CodiceCategoria = C.CodiceCategoria
																	JOIN IngredienteInPiatto AS IIP ON IIP.CodicePiatto = P.CodicePiatto
																	JOIN Ingrediente AS I ON I.CodiceIngrediente = IIP.CodiceIngrediente
																	WHERE P.InMenu = true
																	AND P.CodicePiatto NOT IN
																					(
																									SELECT P.CodicePiatto FROM Piatto AS P
																									JOIN IngredienteInPiatto AS IIP ON IIP.CodicePiatto = P.CodicePiatto
																									JOIN Ingrediente AS I ON I.CodiceIngrediente = IIP.CodiceIngrediente
																									WHERE I.QuantitaInMagazzino < IIP.Quantita
																					)");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getAdminAllDishesByCategory() {
		$stmt = $this->db->prepare ("	SELECT C.Nome AS NomeCategoria, C.CodiceCategoria, P.CodicePiatto, P.Nome, P.InMenu, P.PrezzoAttuale, P.TempoPreparazione, I.Nome AS NomeIngrediente, I.CodiceIngrediente, IIP.Quantita, I.QuantitaInMagazzino FROM Categoria AS C
																	LEFT OUTER JOIN Piatto AS P ON P.CodiceCategoria = C.CodiceCategoria
																	LEFT OUTER JOIN IngredienteInPiatto AS IIP ON IIP.CodicePiatto = P.CodicePiatto
																	LEFT OUTER JOIN Ingrediente AS I ON I.CodiceIngrediente = IIP.CodiceIngrediente");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getUserAllBeverages() {
		$stmt = $this->db->prepare ("	SELECT * FROM Bevanda
																	WHERE InMenu = true
																	AND QuantitaInMagazzino > 0");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getAdminAllBeverages() {
		$stmt = $this->db->prepare ("	SELECT * FROM Bevanda");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function orderCourse($dishes, $beverages, $presenceId) {
		$stmt = $this->db->prepare ("	SELECT COUNT(NumeroPortata) AS PORTATE_IN_PRESENZA FROM Portata
																	WHERE CodicePresenza = ?");
		$stmt->bind_param('i', $presenceId);
		$stmt->execute();
		$result = $stmt->get_result();
		$courseNumber = ($result->fetch_all(MYSQLI_ASSOC)[0]["PORTATE_IN_PRESENZA"]) + 1;

		$stmt = $this->db->prepare (" INSERT INTO Portata (NumeroPortata, CodicePresenza, Orario)
																	values	(?, ?, NOW())");
		$stmt->bind_param('ii', $courseNumber, $presenceId);
		$stmt->execute();

		if (!empty($dishes)) {
			foreach ($dishes as $dish) {
				$stmt = $this->db->prepare (" INSERT INTO PiattoInPortata (NumeroPortata, CodicePresenza, CodicePiatto, Quantita, Pronto)
																			values	(?, ?, ?, ?, ?)");
				$stmt->bind_param('iiiis', $courseNumber, $presenceId, $dish->CodicePiatto, $dish->Quantita, $this->DistantFuture);
				$stmt->execute();
	
				$this->createDishTransaction($dish->CodicePiatto, $dish->Quantita);
			}
		}

		if (!empty($beverages)) {
			foreach ($beverages as $beverage) {
				$stmt = $this->db->prepare (" INSERT INTO BevandaInPortata (NumeroPortata, CodicePresenza, CodiceBevanda, Quantita)
																			values	(?, ?, ?, ?)");
				$stmt->bind_param('iiii', $courseNumber, $presenceId, $beverage->CodiceBevanda, $beverage->Quantita);
				$stmt->execute();
	
				$this->createBeverageTransaction($beverage->CodiceBevanda, -($beverage->Quantita));
			}
		}

		$stmt = $this->db->prepare ("	UPDATE Presenza
																	SET Spesa = Spesa + IFNULL((SELECT SUM(P.PrezzoAttuale * PIP.Quantita) FROM Piatto AS P
																											 JOIN PiattoInPortata AS PIP ON P.CodicePiatto = PIP.CodicePiatto
																											 WHERE PIP.NumeroPortata = ?
																											 AND PIP.CodicePresenza = ?), 0)
																										+ IFNULL((SELECT SUM(B.PrezzoAttuale * BIP.Quantita) FROM Bevanda AS B
																											 JOIN BevandaInPortata AS BIP ON B.CodiceBevanda = BIP.CodiceBevanda
																											 WHERE BIP.NumeroPortata = ?
																											 AND BIP.CodicePresenza = ?), 0)
																	WHERE CodicePresenza = ?");
		$stmt->bind_param('iiiii', $courseNumber, $presenceId, $courseNumber, $presenceId, $presenceId);
		return $stmt->execute();
	}

	//----------- 10 ----------//

	public function showIngredientWarehouseProgress($ingredientId) {
		$stmt = $this->db->prepare ("	SELECT * FROM Transazione
																	WHERE CodiceIngrediente = ?
																	ORDER BY DataOra ASC");
		$stmt->bind_param('i',$ingredientId);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);	
	}

	//----------- 11 ----------//

	public function showBeverageWarehouseProgress($beverageId) {
		$stmt = $this->db->prepare ("	SELECT * FROM Transazione
																	WHERE CodiceBevanda = ?
																	ORDER BY DataOra ASC");
		$stmt->bind_param('i',$beverageId);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	//----------- 12 ----------//
	
	public function showDishOrderProgress($dishId) {
		$stmt = $this->db->prepare ("	SELECT Prezzo, DaData, AData FROM Prezzo
																	WHERE CodicePiatto = ?");
		$stmt->bind_param('i',$dishId);
		$stmt->execute();
		$result = $stmt->get_result();
		$prices = $result->fetch_all(MYSQLI_ASSOC);

		$stmt = $this->db->prepare ("	SELECT FROM PiattoInPortata AS PIP
																	JOIN Presenza AS P ON P.CodicePresenza = PIP.CodicePresenza
																	WHERE CodicePiatto = ?");
		$stmt->bind_param('i',$dishId);
		$stmt->execute();
		$result = $stmt->get_result();
		$orders = $result->fetch_all(MYSQLI_ASSOC);

		return (array("Prezzi" => $prices, "Ordini" => $orders));
	}

	//---------- other ----------//

	public function addNewIngredient($name, $quantity) {
		$stmt = $this->db->prepare ("SELECT Nome FROM Ingrediente WHERE Nome = ? LIMIT 1");
		$stmt->bind_param('s',$name);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			$stmt = $this->db->prepare (" INSERT INTO Ingrediente (Nome, QuantitaInMagazzino)
																		values	(?, 0)");
			$stmt->bind_param('s', $name);
			$stmt->execute();

			$stmt = $this->db->prepare ("SELECT LAST_INSERT_ID() AS INGREDIENT_ID");
			$stmt->execute();
			$result = $stmt->get_result();
			$ingredientId = $result->fetch_all(MYSQLI_ASSOC)[0]["INGREDIENT_ID"];

			$this->createIngredientTransaction($ingredientId, $quantity);
			return array(true, $ingredientId);
		} else {
			return array(false, null);
		}
	}

	public function addNewBeverage($name, $capacity, $price, $quantity) {
		$stmt = $this->db->prepare ("SELECT Nome, Capienza FROM Bevanda WHERE Nome = ? AND Capienza = ? LIMIT 1");
		$stmt->bind_param('si',$name, $capacity);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			$stmt = $this->db->prepare (" INSERT INTO Bevanda (Nome, Capienza, InMenu, PrezzoAttuale, QuantitaInMagazzino)
																		values	(?, ?, true, ?, 0)");
			$stmt->bind_param('sid', $name, $capacity, $price);
			$stmt->execute();

			$stmt = $this->db->prepare ("SELECT LAST_INSERT_ID() AS BEVERAGE_ID");
			$stmt->execute();
			$result = $stmt->get_result();
			$beverageId = $result->fetch_all(MYSQLI_ASSOC)[0]["BEVERAGE_ID"];

			$stmt = $this->db->prepare ("	INSERT INTO Prezzo (CodiceBevanda, DaData, AData, Prezzo)
																		values	(?, NOW(), ?, ?)");
			$stmt->bind_param('isd', $beverageId, $this->DistantFuture, $price);
			$stmt->execute();

			$this->createBeverageTransaction($beverageId, $quantity);
			return array(true, $beverageId);
		} else {
			return array(false, null);
		}
	}

	public function addNewCategory($name) {
		$stmt = $this->db->prepare ("SELECT Nome FROM Categoria WHERE Nome = ? LIMIT 1");
		$stmt->bind_param('s',$name);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			$stmt = $this->db->prepare (" INSERT INTO Categoria (Nome)
																		values	(?)");
			$stmt->bind_param('s', $name);
			$stmt->execute();

			$stmt = $this->db->prepare ("SELECT LAST_INSERT_ID() AS CATEGORY_ID");
			$stmt->execute();
			$result = $stmt->get_result();
			$categoryId = $result->fetch_all(MYSQLI_ASSOC)[0]["CATEGORY_ID"];			

			return array(true, $categoryId);
		} else {
			return array(false, null);
		}
	}

	public function checkUserLogin($email, $password) {
		$stmt = $this->db->prepare ("	SELECT Email, Passphrase FROM Utente
																	WHERE Email=?
																	LIMIT 1");
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		
		if (empty($result)) {
			return array(false, null);
		} else {
			$user = $result[0];
			if (password_verify($password, $user["Passphrase"])) {
				return array(true, $user["Email"]);
			}
		}
	}

	public function getUserInformation($email) {
		$stmt = $this->db->prepare ("	SELECT * FROM Utente
																	WHERE Email=?
																	LIMIT 1");
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		
		if (empty($result)) {
			return array(false, null);
		} else {
			$user = $result[0];
			return array(true, $user);
		}
	}

	public function getUserActivePresence($email) {
		$stmt = $this->db->prepare ("	SELECT P.* FROM Utente AS U
																	JOIN Presenza AS P ON P.EmailUtente = U.Email
																	WHERE Email=?
																	AND P.DOArrivo < NOW()
																	AND P.DOUscita > NOW()
																	LIMIT 1");
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		
		if (empty($result)) {
			return array(false, null);
		} else {
			$presence = $result[0];
			return array(true, $presence);
		}
	}

	public function getPresence($presenceId) {
		$stmt = $this->db->prepare ("	SELECT P.*, U.*, T.Nome AS NomeTavolo FROM Presenza AS P
																	LEFT OUTER JOIN Utente AS U ON P.EmailUtente = U.Email
																	JOIN Tavolo AS T ON T.CodiceTavolo = P.CodiceTavolo
																	WHERE P.CodicePresenza = ?");
		$stmt->bind_param('i',$presenceId);
		$stmt->execute();
		$result = $stmt->get_result();
		$result =  $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			return (array(false, null));
		} else {
			return (array(true, $result[0]));
		}
	}

	public function getAllActivePresences() {
		$stmt = $this->db->prepare ("	SELECT P.*, U.*, T.Nome AS NomeTavolo FROM Presenza AS P
																	LEFT OUTER JOIN Utente AS U ON P.EmailUtente = U.Email
																	JOIN Tavolo AS T ON T.CodiceTavolo = P.CodiceTavolo
																	WHERE DOArrivo < NOW()
																	AND DOUscita > NOW()
																	ORDER BY P.DOArrivo DESC");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getAllPresences() {
		$stmt = $this->db->prepare ("	SELECT P.*, U.*, T.Nome AS NomeTavolo FROM Presenza AS P
																	LEFT OUTER JOIN Utente AS U ON P.EmailUtente = U.Email
																	JOIN Tavolo AS T ON T.CodiceTavolo = P.CodiceTavolo
																	ORDER BY (P.DOUscita > NOW()) DESC, P.DOArrivo DESC");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getAllUserPresences($email) {
		$stmt = $this->db->prepare ("	SELECT Presenza.*, Utente.*, Tavolo.Nome AS NomeTavolo FROM Presenza 
																	JOIN Utente ON Presenza.EmailUtente = Utente.Email
																	JOIN Tavolo ON Tavolo.CodiceTavolo = Presenza.CodiceTavolo
																	WHERE EmailUtente = ?");
		$stmt->bind_param('s',$email);
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getBeverageInformation($beverageId) {
		$stmt = $this->db->prepare ("	SELECT Nome, CodiceBevanda, QuantitaInMagazzino FROM Bevanda
																	WHERE CodiceBevanda = ?");
		$stmt->bind_param('i', $beverageId);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			return array(false, null);
		} else {
			return array(true, $result[0]);
		}
	}

	public function getIngredientInformation($ingredientId) {
		$stmt = $this->db->prepare ("	SELECT Nome, CodiceIngrediente, QuantitaInMagazzino FROM Ingrediente
																	WHERE CodiceIngrediente = ?");
		$stmt->bind_param('i', $IngredientId);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			return array(false, null);
		} else {
			return array(true, $result[0]);
		}
	}

	public function removeDishFromMenu($dishId) {
		$stmt = $this->db->prepare ("	UPDATE Piatto
																	SET InMenu = false
																	WHERE CodicePiatto = ?
																	AND InMenu = true");
		$stmt->bind_param('i', $dishId);
		return $stmt->execute();
	}

	public function removeBeverageFromMenu($beverageId) {
		$stmt = $this->db->prepare ("	UPDATE Bevanda
																	SET InMenu = false
																	WHERE CodiceBevanda = ?
																	AND InMenu");
		$stmt->bind_param('i', $beverageId);
		return $stmt->execute();
	}

	public function restoreDishInMenu($dishId) {
		$stmt = $this->db->prepare ("	UPDATE Piatto
																	SET InMenu = true
																	WHERE CodicePiatto = ?
																	AND InMenu = false");
		$stmt->bind_param('i', $dishId);
		return $stmt->execute();
	}

	public function restoreBeverageInMenu($beverageId) {
		$stmt = $this->db->prepare ("	UPDATE Bevanda
																	SET InMenu = true
																	WHERE CodiceBevanda = ?
																	AND InMenu = false");
		$stmt->bind_param('i', $beverageId);
		return $stmt->execute();
	}

	public function getCategories() {
		$stmt = $this->db->prepare ("	SELECT * FROM Categoria");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getIngredients() {
		$stmt = $this->db->prepare ("	SELECT * FROM Ingrediente");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getTables() {
		$stmt = $this->db->prepare ("	SELECT Tavolo.* FROM Tavolo
																	ORDER BY Tavolo.Nome ASC");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function getTable($tableId) {
		$stmt = $this->db->prepare ("	SELECT Tavolo.* FROM Tavolo
																	WHERE Tavolo.CodiceTavolo = ?");
		$stmt->bind_param('i',$tableId);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);

		if (empty($result)) {
			return array(false, null);
		} else {
			return array(true, $result[0]);
		}
	}

	public function addNewTable($name, $maxPeople) {
		$stmt = $this->db->prepare ("SELECT Nome FROM Tavolo WHERE Nome = ? LIMIT 1");
		$stmt->bind_param('s',$name);
		$stmt->execute();
		$result = $stmt->get_result();
		$result = $result->fetch_all(MYSQLI_ASSOC);
		if (empty($result)) {
			$stmt = $this->db->prepare (" INSERT INTO Tavolo (Nome, MaxPosti, InRistorante, PostiOccupati)
																		values	(?, ?, true, 0)");
			$stmt->bind_param('si', $name, $maxPeople);
			$stmt->execute();

			$stmt = $this->db->prepare ("SELECT LAST_INSERT_ID() AS TABLE_ID");
			$stmt->execute();
			$result = $stmt->get_result();
			$tableId = $result->fetch_all(MYSQLI_ASSOC)[0]["TABLE_ID"];

			return array(true, $tableId);
		} else {
			return array(false, null);
		}
	}

	public function changeTableMaxPeople($tableId, $maxPeople) {
		$stmt = $this->db->prepare ("	UPDATE Tavolo
																	SET MaxPosti = ?
																	WHERE CodiceTavolo = ?
																	AND PostiOccupati = 0");
		$stmt->bind_param('ii', $maxPeople, $tableId);
		return $stmt->execute();
	}

	public function removeTableFromRestaurant($tableId) {
		$stmt = $this->db->prepare ("	UPDATE Tavolo
																	SET InRistorante = false
																	WHERE CodiceTavolo = ?
																	AND InRistorante");
		$stmt->bind_param('i', $tableId);
		return $stmt->execute();
	}

	public function restoreTableInRestaurant($tableId) {
		$stmt = $this->db->prepare ("	UPDATE Tavolo
																	SET InRistorante = true
																	WHERE CodiceTavolo = ?
																	AND InRistorante = false");
		$stmt->bind_param('i', $tableId);
		return $stmt->execute();
	}

	public function getDishesNotReady() {
		$stmt = $this->db->prepare ("	SELECT  Piatto.CodicePiatto, Piatto. Nome AS NomePiatto, Piatto.TempoPreparazione,
																					Ingrediente.CodiceIngrediente, Ingrediente.Nome AS NomeIngrediente,
																					IIP.Quantita AS QuantitaIngrediente,
																					PIP.Quantita AS NumeroPiatti, PIP.Pronto,
																					Portata.Orario AS OrarioOrdinazione, Portata.NumeroPortata, Portata.CodicePresenza,
																					Tavolo.Nome AS Tavolo
																	FROM Piatto
																	JOIN PiattoInPortata 			AS PIP 	ON PIP.CodicePiatto = Piatto.CodicePiatto
																	JOIN IngredienteInPiatto 	AS IIP 	ON IIP.CodicePiatto = Piatto.CodicePiatto
																	JOIN Portata 											ON (PIP.NumeroPortata = Portata.NumeroPortata AND PIP.CodicePresenza = Portata.CodicePresenza)
																	JOIN Presenza 										ON Portata.CodicePresenza = Presenza.CodicePresenza
																	JOIN Tavolo 											ON Tavolo.CodiceTavolo = Presenza.CodiceTavolo
																	JOIN Ingrediente 									ON IIP.CodiceIngrediente = Ingrediente.CodiceIngrediente
																	WHERE PIP.Pronto > NOW()
																	ORDER BY Portata.Orario, Piatto.Nome");
		$stmt->execute();
		$result = $stmt->get_result();
		return $result->fetch_all(MYSQLI_ASSOC);
	}

	public function setDishReady($presenceId, $courseNumber, $dishId) {
		$stmt = $this->db->prepare ("	UPDATE PiattoInPortata
																	SET Pronto = NOW()
																	WHERE CodicePresenza = ?
																	AND NumeroPortata = ?
																	AND CodicePiatto = ?
																	AND Pronto > NOW()");
		$stmt->bind_param('iii', $presenceId, $courseNumber, $dishId);
		return $stmt->execute();
	}

}
	
?>