<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: GET, POST");
header("Access-Control-Allow-Headers: Content-Type, *");

// server should keep session data for AT LEAST 1 hour
ini_set('session.gc_maxlifetime', 3600);

// each client should remember their session id for EXACTLY 1 hour
session_set_cookie_params(3600);

session_start();

$rootPassword = $_ENV["MYSQL_ROOT_PASSWORD"];
$dbPort = $_ENV["MYSQL_PORT"];
$dbHost = $_ENV["MYSQL_HOST"];
$runDir = $_ENV["RUN_DIR"];

require_once($runDir."/Database/database.php");
require_once($runDir."/helpFunctions.php");
$dbh = new DatabaseHelper($dbHost, "root", $rootPassword, "basiDiDati", $dbPort);
?>