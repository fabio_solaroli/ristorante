# Progetto Basi di Dati

### Come eseguire l'applicativo

Dopo aver installato [Docker](https://docs.docker.com/get-docker/), andare sulla cartella root del progetto ed eseguire il comando `docker-compose up -d --build`

### Testare l'applicativo

* `localhost/app/` per l'app con cui l'utente può ordinare dal menù
* `localhost/app/admin` per la zona dell'amministratore
