export function cloneObject(object) {
	return JSON.parse(JSON.stringify(object));
}

export async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

export function groupArrayOfObjects(list, key) {
  return list.reduce(function(rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export function fromMsToMinutes(ms) {
	return (ms / 60 / 1000)
}

export function formatTime(ms) {
	return `${Math.floor(fromMsToMinutes(ms) / 60)} ore e ${Math.floor(fromMsToMinutes(ms) % 60)} minuti`
}

export function dateToHHMM(date) {
	return date?.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").substring(0,5)
}

export function formatQuantity(quantity, type) {
	if (type === 'liquid') {
		return quantity >= 1000 ? `${quantity/1000} l` : `${quantity} ml`
	} else if (type === 'solid') {
		return quantity >= 1000 ? `${quantity/1000} kg` : `${quantity} g`
	}
}

export function scrollToElm(container, elm){
  var pos = getRelativePos(elm);
  scrollTo( container, pos.top , 0.5);
}

function getRelativePos(elm){
  var pPos = elm.parentNode.getBoundingClientRect(),
      cPos = elm.getBoundingClientRect(),
      pos = {};

  pos.top    = cPos.top    - pPos.top + elm.parentNode.scrollTop
  pos.right  = cPos.right  - pPos.right
  pos.bottom = cPos.bottom - pPos.bottom
  pos.left   = cPos.left   - pPos.left

  return pos;
}
    
function scrollTo(element, to, duration, onDone) {
    var start = element.scrollTop,
        change = to - start,
        startTime = performance.now(),
        now, elapsed, t;

    function animateScroll(){
        now = performance.now();
        elapsed = (now - startTime)/1000;
        t = (elapsed/duration);

        element.scrollTop = start + change * easeInOutQuad(t) - 5;

        if( t < 1 )
            window.requestAnimationFrame(animateScroll);
        else
            onDone && onDone();
    };

    animateScroll();
}

function easeInOutQuad(t){ return t<.5 ? 2*t*t : -1+(4-2*t)*t };