export const origin = 'api';

async function sendData(requestType, path, body) {
	return fetch(origin + path, {
		method: requestType,
		crossDomain: true,
		xhrFields: { withCredentials: true },
		mode: 'cors',
		body: body
	}).then(res => res.json())
}

async function post(path, body) {
	return sendData('POST', path, body)
}

async function put(path, body) {
	return sendData('PUT', path, body)
}

async function get(path) {
	return fetch(origin + path, {
		crossDomain: true,
		xhrFields: { withCredentials: true }
	}).then(res => res.json())
}

async function remove(path) {
	return fetch(origin + path, {
    method: 'DELETE'
  }).then(res => res.json())
}

//------------------------------------//
//------------- admin ----------------//
//------------------------------------//

export async function checkAdminLoggedIn() {
	return get('/admin.php')
}

export async function logAdmin(credentials) {
	const body = new FormData();
	body.append("username", credentials.username);
	body.append("password", credentials.password);

	return post('/admin.php', body)
}

export async function logOutAdmin() {
	return remove('/admin.php')
}

//------------------------------------//
//------------- menu -----------------//
//------------------------------------//

export async function getAllDishesByCategory() {
	return get('/menu.php/dish/category')
}

export async function getAdminAllDishesByCategory() {
	return get('/menu.php/dish/category/admin')
}

export async function getAllBeverages() {
	return get('/menu.php/beverage')
}

export async function getAdminAllBeverages() {
	return get('/menu.php/beverage/admin')
}

export async function removeDishFromMenu(dishId) {
	return remove(`/menu.php/menu/dish/${dishId}`);
}

export async function restoreDishInMenu(dishId) {
	return put(`/menu.php/menu/dish/${dishId}`)
}

export async function removeBeverageFromMenu(beverageId) {
	return remove(`/menu.php/menu/beverage/${beverageId}`);
}

export async function restoreBeverageInMenu(beverageId) {
	return put(`/menu.php/menu/beverage/${beverageId}`)
}

export async function addNewIngredient(ingredient) {
	const body = new FormData();
	body.append("name", ingredient.name);
	body.append("quantity", ingredient.quantity);

	return post(`/menu.php/ingredient`, body)
}

export async function addNewCategory(name) {
	const body = new FormData();
	body.append("name", name);

	return post(`/menu.php/category`, body)
}

export async function addNewDish(dish) {
	const body = new FormData();
	body.append("name", dish.name);
	body.append("ingredients", JSON.stringify(dish.ingredients.map(d => {return{id: d.id, quantity: d.quantity*d.unitMeasure}})));
	body.append("category", dish.category);
	body.append("price", dish.price);
	body.append("preparationTime", dish.preparationTime);
	
	return post(`/menu.php/dish`, body)
}

export async function addNewBeverage(beverage) {
	const body = new FormData();
	body.append("name", beverage.name);
	body.append("quantity", beverage.quantity);
	body.append("capacity", beverage.capacity);
	body.append("price", beverage.price);
	
	return post(`/menu.php/beverage`, body)
}

export async function editDishPrice(dishId, price) {
	const body = new FormData();
	body.append("dishId", dishId)
	body.append("price", price)

	return post(`/menu.php/dish/price`, body)
}

export async function editBeveragePrice(beverageId, price) {
	const body = new FormData();
	body.append("beverageId", beverageId)
	body.append("price", price)

	return post(`/menu.php/beverage/price`, body)
}

export async function getCategories() {
	return get(`/menu.php/category`)
}

export async function getIngredients() {
	return get(`/menu.php/ingredient`)
}

//------------------------------------//
//------------ presence --------------//
//------------------------------------//

export async function addUserToPresence(email, presenceId) {
	const body = new FormData();
	body.append("email", email);
	body.append("presenceId", presenceId);

	return post(`/presence.php/user`, body)
}

export async function startPresence(presence) {
	const body = new FormData();
	body.append("table", presence.table.CodiceTavolo);
	body.append("nPeople", presence.nPeople);

	return post('/presence.php', body)
}

export async function endPresence(presenceId) {
	return remove(`/presence.php/${presenceId}`)
}

export async function getPresence(presenceId) {
	return get(`/presence.php/${presenceId}`)
}

export async function getPresenceDetails(presenceId) {
	return get(`/presence.php/details/${presenceId}`)
}

export async function getActivePresences() {
	return get('/presence.php/active')
}

export async function getAllPresences() {
	return get('/presence.php/all')
}

export async function getAllUserPresences(email) {
	return get(`/presence.php/user/${email}/all`)
}

export async function isPresenceActive() {
	return get(`/presence.php`)
}

export async function orderCourse(dishes, beverages) {
	const body = new FormData();
	body.append("dishes", JSON.stringify(dishes));
	body.append("beverages", JSON.stringify(beverages));
	
	return post(`/presence.php/course`, body)
}

//------------------------------------//
//------------- kitchen --------------//
//------------------------------------//

export async function getKitchenDishes() {
	return get('/kitchen.php')
}

export async function setDishReady(presenceId, courseNumber, dishId) {
	const body = new FormData();
	body.append("CodicePresenza", presenceId);
	body.append("NumeroPortata", courseNumber);
	body.append("CodicePiatto", dishId);
	
	return post(`/kitchen.php`, body)
}

//------------------------------------//
//------------ restaurant ------------//
//------------------------------------//

export async function getTables() {
	return get(`/restaurant.php/table`)
}

export async function getTable(tableId) {
	return get(`/restaurant.php/table/${tableId}`)
}

export async function addNewTable(table) {
	const body = new FormData();
	body.append("name", table.name);
	body.append("maxPeople", table.maxPeople);
	
	return post(`/restaurant.php/table`, body)
}

export async function changeTableMaxPeople(tableId, maxPeople) {
	const body = new FormData();
	body.append("maxPeople", maxPeople);
	
	return post(`/restaurant.php/table/${tableId}`, body)
}

export async function removeTableFromRestaurant(tableId) {
	return remove(`/restaurant.php/table/${tableId}`);
}

export async function restoreTableInRestaurant(tableId) {
	return put(`/restaurant.php/table/${tableId}`)
}

//------------------------------------//
//-------------- user ----------------//
//------------------------------------//

export async function postUser(user) {
	const body = new FormData();
	body.append("email", user.email);
	body.append("password", user.password);
	body.append("name", user.name);
	body.append("surname", user.surname);

	return post('/user.php/new', body)
}

export async function checkUserLoggedIn() {
	return get('/user.php')
}

export async function getUserInformation(email) {
	return get(`/user.php/${email}`)
}

export async function logUser(credentials) {
	const body = new FormData();
	body.append("email", credentials.email);
	body.append("password", credentials.password);

	return post('/user.php', body)
}

export async function logOutUser() {
	return remove('/user.php')
}

//------------------------------------//
//------------- warehouse ------------//
//------------------------------------//

export async function checkWarehouse() {
	return get('/warehouse.php')
}

export async function getIngredientWarehouseProgress(ingredientId) {
	return get(`/warehouse.php/ingredient/progress/${ingredientId}`)
}

export async function getBeverageWarehouseProgress(beverageId) {
	return get(`/warehouse.php/beverage/progress/${beverageId}`)
}

export async function getIngredientInformation(ingredientId) {
	return get(`/warehouse.php/ingredient/${ingredientId}`)
}

export async function getBeverageInformation(beverageId) {
	return get(`/warehouse.php/beverage/${beverageId}`)
}

export async function createIngredintTransaction(ingredientId, quantity) {
	const body = new FormData();
	body.append("ingredientId", ingredientId);
	body.append("quantity", quantity);

	return post('/warehouse.php/ingredient/transaction', body)
}

export async function createBeverageTransaction(beverageId, quantity) {
	const body = new FormData();
	body.append("beverageId", beverageId);
	body.append("quantity", quantity);

	return post('/warehouse.php/beverage/transaction', body)
}


//-----------------------------------//