import React, { useState, useEffect } from "react";
import { useHistory, useParams } from "react-router";
import { Container, Header, Divider, Button, List, Card } from "semantic-ui-react";
import { endPresence, getPresenceDetails } from "../common/api";
import { dateToHHMM, formatTime, groupArrayOfObjects } from "../common/helpFunctions";

export function PresenceDetails() {

	const { presenceId } = useParams()

	const history = useHistory()

	const [presence, setPresence] = useState({})

	const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }

	useEffect(() => {
		getPresenceDetails(presenceId).then(res => {
			if (res.status === 200) {
				const result = Object.values(groupArrayOfObjects(res.data, "CodicePresenza")).map(p => {
					return ({
						id: p[0].CodicePresenza,
						user: p[0].EmailUtente === null ? null : {email: p[0].EmailUtente, name: p[0].NomeUtente},
						table: p[0].NomeTavolo,
						people: p[0].NumeroPersone,
						charge: p[0].Spesa,
						entry: new Date(p[0].DOArrivo.replace(" ", "T")),
						exit: new Date(p[0].DOUscita.replace(" ", "T")),
						courses: p[0].NumeroPortata === null ? null : Object.values(groupArrayOfObjects(p, "NumeroPortata")).map(c => {
							return ({
								courseNumber: c[0].NumeroPortata,
								time: new Date(c[0].Orario.replace(" ", "T")),
								plates: Object.values(groupArrayOfObjects(c.map(p => {
									return {
										type: p.Tipo,
										id: p.Codice,
										name: p.Nome, 
										quantity: p.Quantita,
										ready: new Date(p.Pronto?.replace(" ", "T") || ''),
										price: p.Prezzo}
									}
								), "type")).flatMap(e => e)
							})
						})
					})
				})
				setPresence(result[0])
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})	
	}, [history, presenceId])

	return (
		<Container className='column-scroll'>
			<Header as='h1'>Dettagli Presenza</Header>
			<Header as='h2'>{`${presence.user ? `${presence.user.name} (${presence.user.email || ''})` : 'Ospite'} - ${presence.entry?.toLocaleDateString('it-IT', options) || ''}`}</Header>
			<p>Numero Persone: {presence.people}</p>
			<p>Tavolo: {presence.table}</p>
			<p>Conto: {presence.charge || 0} €</p>
			<p>Arrivo: {dateToHHMM(presence.entry)}</p>
			{presence.exit > new Date() ?
				<>
					<Divider hidden/>
					<Button inverted color='red' className='end' onClick={(e) => {
						endPresence(presence.id);
						history.goBack()
					}}>Termina presenza</Button>
				</>
			: <>
				<p>Uscita: {dateToHHMM(presence.exit)}</p>
				<p>Tempo di permanenza: {formatTime(presence.exit - presence.entry)}</p>
			</>}
			<Header as='h2'>Portate</Header>
			{presence.courses == null ? 'Nessuna portata ordinata' :
				presence.courses.map(c => {
					return <CourseDetails key={c.courseNumber} course={c}/>
				})
			}
		</Container>
	)
}

function CourseDetails(props) {

	const history = useHistory()

	return (
		<Card fluid>
			<Card.Content className='background'>
				<Card.Header as='h3' className='background'>{props.course.courseNumber}ª portata - {dateToHHMM(props.course.time)} - {props.course.plates.map(p => p.price * p.quantity).reduce(((a, b) => +a + +b), 0)} €</Card.Header>
			</Card.Content>
			<Card.Content>
				<List>
					{props.course.plates.map(p => {
						return (
							<List.Item key={`${p.type}-${p.id}`}  onClick={() => history.push(`/admin/menu/${p.type === 'PIATTO' ? 'dish' : 'beverage'}/${p.id}`)}>
								<List.Icon name={p.type === 'PIATTO' ? 'food' : 'beer'}/>
								<List.Content>
									<List.Header as='a' className='marginBottom'>{p.name}</List.Header>
									<List.Description>
										<p>Quantita: {p.quantity}</p>
										<p>Costo: {p.price*p.quantity} €</p>
										<p>{p.type === 'PIATTO' ? (p.ready > new Date() ? 'Piatto non ancora pronto' : `Pronto alle ${dateToHHMM(p.ready)}`) : null}</p>
									</List.Description>
								</List.Content>
							</List.Item>
						)
					})}
				</List>
			</Card.Content>
		</Card>
	)
}