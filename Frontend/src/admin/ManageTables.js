import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Container, Header, Segment, Icon, Divider, Button } from "semantic-ui-react";
import { getTables, restoreTableInRestaurant } from "../common/api";

export function ManageTables() {

	const history = useHistory()

	const [activeTables, setActiveTables] = useState([])
	const [removedTables, setRemovedTables] = useState([])
	const [update, setUpdate] = useState(new Date())

	useEffect(() => {
		getTables().then(res => {
			if (res.status === 200) {
				setActiveTables(res.data.filter(t => t.InRistorante))
				setRemovedTables(res.data.filter(t => !t.InRistorante))
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [update, history])

	return (
		<Container className='column-scroll'>
			<Header as='h1'>Tavoli</Header>
				{activeTables.map(t => {
					return(
						<Segment className='flex column' color={t.PostiOccupati > 0 ? (t.MaxPosti === +t.PostiOccupati ? 'red' : 'orange') : 'green'} key={`${t.CodiceTavolo}`} onClick={() => {if (!t.PostiOccupati > 0 && t.InRistorante) {history.push(`/admin/tables/modifyTable/${t.CodiceTavolo}`)}}}>
							<Container className='noWrap center space-between'>
								<Container className='noWrap center'>
									<Icon className='marginRight' name='circle' size='small' color={t.PostiOccupati > 0 ? (t.MaxPosti === +t.PostiOccupati ? 'red' : 'orange') : 'green'}/>
									<Header as='h3' className='noMargin'>{t.Nome}</Header>
								</Container>
								<Container className='flex center justify-end'>
									<p className='noMargin'>
										{t.PostiOccupati > 0 ? "Occupato" : "Libero"}</p>
									<p className='marginLeft'>
										{` (${t.PostiOccupati}/${t.MaxPosti} posti)`}
									</p>
								</Container>
								<Icon name='angle right' size='large' color={t.PostiOccupati > 0 ? 'grey' : 'blue'}/>
							</Container>
						</Segment>
					)
				})}
			<Divider hidden/>
			<Container className='flex column'>
				<Button className='end' inverted color='blue' onClick={() => {history.push(`/admin/tables/addTable`)}}>Aggiungi Tavolo</Button>
			</Container>
			{removedTables.length !== 0 ?
				<>
					<Divider/>
					<Header as='h1'>Tavoli Rimossi</Header>
					{removedTables.map(t => {
						return(
							<Segment key={t.CodiceTavolo} className='noWrap center space-between'>
								<Header as='h3' className='noMargin'>{t.Nome}</Header>
								<Container className='noWrap center justify-end'>
									<p className='noMargin marginRight'>{`${t.MaxPosti} posti`}</p>
									<Button inverted color='green' onClick={() => {restoreTableInRestaurant(t.CodiceTavolo); setUpdate(new Date())}}>Reinserisci</Button>
								</Container>
							</Segment>
						)
					})}
				</>
			: null}
			<Divider hidden/>
		</Container>
	)
}