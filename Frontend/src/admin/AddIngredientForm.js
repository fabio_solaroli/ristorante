import { useState } from "react";
import { useHistory } from "react-router";
import { Container, Form, Button, Message, Label, Divider } from "semantic-ui-react";
import { addNewIngredient } from "../common/api";
import { cloneObject } from "../common/helpFunctions";

export function AddIngredientForm() {

	const history = useHistory()

	const [ingredient, setIngredient] = useState({name: "", quantity: 0})
	const [error, setError] = useState({active: false, message: ""})

	const updateIngredient = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};
		setIngredient(Object.assign(cloneObject(ingredient), result))
	}

	const handleSubmit = async (event) => {
		addNewIngredient(ingredient).then(response => {
			switch (response.status) {
				case 201:
					history.replace(`/admin/warehouse/ingredient/${response.data}`)
					break;
				case 401:
					history.push(`/admin/login`)
					break;
				default:
					setError({active: true, message: response.message})
					break;
			}
		})
	}

	return(
		<Container>
			<Form onSubmit={handleSubmit} error={error.active}>
				<AddIngredientInput updateIngredient={updateIngredient} ingredient={ingredient} error={error}/>
				<Divider hidden/>
				<Container className='flex space-between'>
					<Button basic color='grey' type='submit' onClick={(e) => {e.preventDefault(); history.goBack()}}>Annulla</Button>
					<Button positive type='submit'>Aggiungi</Button>
				</Container>
			</Form>
		</Container>
	)
}

function AddIngredientInput(props) {
	return (
		<Container>
			<Form.Input required id='name' size='small' onChange={props.updateIngredient('name')} value={props.ingredient.name} label='Nome'/>
			<Message warning error={props.error.active} content={props.error.message}/>
			<Form.Input required id='quantity' size='small' onChange={props.updateIngredient('quantity')} value={props.ingredient.quantity} label='Quantita' type='number' min={0}>
				<input/>
				<Label>gr</Label>
			</Form.Input>
		</Container>
	)
}