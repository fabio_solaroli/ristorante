import { useHistory } from "react-router";
import { Container, Header, List, Button, Divider, Card, Popup } from "semantic-ui-react";
import { endPresence } from "../common/api";
var QRCode = require('qrcode.react');

export function ShortPresence(props) {

	const history = useHistory()

	return (
		<List.Item>
			<Card fluid className='marginTop' onClick={() => history.push(`/admin/presences/${props.presence.CodicePresenza}`)}>
				<Card.Content className='background'>
					<Popup
						content= {
							<QRCode
								onClick={(e) => {e.preventDefault(); e.stopPropagation()}}
								value={props.presence.CodicePresenza.toString()}
								size={200}
							/>
						}
						hideOnScroll
						on='click'
						popper={{ id: 'popper-container', style: { zIndex: 2000 } }}
						trigger={
							<QRCode
								onClick={(e) => {e.preventDefault(); e.stopPropagation()}}
								value={props.presence.CodicePresenza.toString()}
								size={50} className='floatingTop'
							/>
						}
					/>
					<Container className='flex column' textAlign='right'>
						<p className='noMargin'>{`Arrivo: ${props.presence.DOArrivo}`}</p>
						{new Date(props.presence.DOUscita.replace(" ", "T")) < new Date() ?
							<p>{`Uscita: ${props.presence.DOUscita}`}</p>
						: <>
							<Button size='tiny' color='red' className='end marginTop' onClick={(e) => {
								e.preventDefault()
								e.stopPropagation()
								endPresence(props.presence.CodicePresenza);
								props.setUpdate(new Date())
							}}>Termina presenza</Button>
					</> }
					</Container>
					<Divider className='noMargin marginBottom' hidden/>
					<Header as='h2' textAlign='center'>
						{ props.presence.EmailUtente !== null ?
								`${props.presence.Nome} ${props.presence.Cognome}`
						:
							"Ospite"
						}
					</Header>
				</Card.Content>
				<Card.Content className='flex column'>
					<Container className='flex column space-between'>
						<Container className='flex space-between'><Header sub>Numero Persone:</Header>{props.presence.NumeroPersone}</Container>
						<Divider/>
						<Container className='flex space-between'><Header sub>Tavolo:</Header>{props.presence.NomeTavolo}</Container>
						<Divider/>
						<Container className='flex space-between'><Header sub>Spesa:</Header>{props.presence.Spesa} €</Container>
					</Container>
				</Card.Content>
			</Card>
		</List.Item>
	);
}