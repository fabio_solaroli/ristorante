import { useState } from "react";
import { useHistory, useParams } from "react-router";
import { Container, Form, Button, Message, Label, Dropdown } from "semantic-ui-react";
import { addNewBeverage } from "../common/api";
import { cloneObject } from "../common/helpFunctions";

export function AddBeverageForm() {

	const history = useHistory()

	const { previousLocation } = useParams()

	const [beverage, setBeverage] = useState({name: "", quantity: 0, capacity: 0, price: 0})
	const [measureUnit, setMeasureUnite] = useState(1)
	const [error, setError] = useState({active: false, message: ""})

	const updateBeverage = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};
		setBeverage(Object.assign(cloneObject(beverage), result))
	}

	const options = [
		{key: 'ml', text: 'ml', value: 1},
		{key: 'l', text: 'l', value: 1000}
	]

	const handleSubmit = async (event) => {
		addNewBeverage(Object.assign(cloneObject(beverage), {capacity: beverage.capacity*measureUnit})).then(response => {
			switch (response.status) {
				case 201:
					if (previousLocation === 'menu') {
						history.replace(`/admin/menu/beverage/${response.data}`)
					} else {
						history.replace(`/admin/warehouse/beverage/${response.data}`)
					}
					break;
				case 401:
					history.replace('/admin/login')
					break;
				default:
					setError({active: true, message: response.message})
					break;
			}
		})
	}

	return(
		<Container>
			<Form onSubmit={handleSubmit} error={error.active}>
				<Form.Input required id='name' size='small' onChange={updateBeverage('name')} value={beverage.name} label='Nome'/>
				<Message warning error={error.active} content={error.message}/>
				<Form.Input required id='quantity' size='small' onChange={updateBeverage('quantity')} value={beverage.quantity} label='Quantita' type='number' min={0}>
					<input />
					<Label>Bottiglie</Label>
				</Form.Input>
				<Form.Input required id='price' size='small' onChange={updateBeverage('price')} value={beverage.price} label='Prezzo' type='number' min={1} step={0.01}>
					<input />
					<Label>€</Label>
				</Form.Input>
				<Form.Input required id='capacity' size='small' onChange={updateBeverage('capacity')} value={beverage.capacity} label='Capacità' type='number' min={1}>
					<input />
					<Label>
						<Dropdown
							options={options}
							value={measureUnit}
							onChange={(e, d) => setMeasureUnite(d.value)}
						/>
					</Label>
				</Form.Input>
				<Container className='flex space-between'>
					<Button basic color='grey' type='submit' onClick={(e) => {e.preventDefault(); history.goBack()}}>Annulla</Button>
					<Button positive type='submit'>Aggiungi</Button>
				</Container>
			</Form>
		</Container>
	)
}