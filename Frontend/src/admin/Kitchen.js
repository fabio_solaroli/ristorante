import { useEffect, useState } from "react"
import { useHistory } from "react-router"
import { Button, Card, Container, Divider, Header, List } from "semantic-ui-react"
import { getKitchenDishes, setDishReady } from "../common/api"
import { dateToHHMM, formatQuantity, formatTime, fromMsToMinutes, groupArrayOfObjects } from "../common/helpFunctions"


export function Kitchen() {

	const history = useHistory()

	const [dishes, setDishes] = useState([])
	const [update, setUpdate] = useState(new Date())

	useEffect(() => {
		getKitchenDishes().then(res => {
			if (res.status === 200) {
				const result = Object.values(groupArrayOfObjects(res.data, 'CodicePresenza')).flatMap(p1 => {
					return Object.values(groupArrayOfObjects(p1, 'NumeroPortata')).flatMap(p2 => {
							return Object.values(groupArrayOfObjects(p2, 'CodicePiatto')).map(p => {
								return ({
									dishId: p[0].CodicePiatto,
									dishName: p[0].NomePiatto,
									preparationTime: p[0].TempoPreparazione,
									dishNumber: p[0].NumeroPiatti,
									ordinationTime: new Date(p[0].OrarioOrdinazione.replace(" ", "T")),
									courseNumber: p[0].NumeroPortata,
									presenceId: p[0].CodicePresenza,
									table: p[0].Tavolo,
									ingredients: p.map( i => {
										return({
											id: i.CodiceIngrediente,
											name: i.NomeIngrediente,
											quantity: i.QuantitaIngrediente,
											warehouseQuantity: i.QuantitaInMagazzino
										})
									})
									
								})
						})
					})
				})
				setDishes(result.sort((a, b) => a.ordinationTime > b.ordinationTime))
			} else if (res.status === 401) {
				history.push(`/admin/login`)
			}
		})
	}, [update, history])

	
	return (
		<Container className='column-scroll'>
			<Header as='h1'>Cucina</Header>
			{dishes.length === 0 ?
				<Header as='h2'>Nessun nuovo ordine</Header>
			: dishes.map(d => {
				const timePassed = fromMsToMinutes(new Date() - d.ordinationTime)
				return (
					<Card fluid key={`${d.dishId}-${d.courseNumber}-${d.presenceId}`}>
						<Card.Content textAlign='center' className={`background-${timePassed < 20 ? 'green' : (timePassed < 60 ? 'orange' : 'red')}`}>
							<Card.Header>{d.dishName} × {d.dishNumber}</Card.Header>
						</Card.Content>
						<Card.Content className='noWrap'>
							<List bulleted className='width50'>
								{d.ingredients.map(i => {
									return (<List.Item key={i.id}>{i.name} ×{formatQuantity(i.quantity * d.dishNumber, 'solid')}</List.Item>)
								})}
							</List>
							<Container className='flex column'>
								<p className='end'>Ordinato da {formatTime(new Date() - d.ordinationTime)} (alle {dateToHHMM(d.ordinationTime)})</p>
								<p className='end'>Tempo di preparazione: {d.dishNumber * d.preparationTime} minuti</p>
								<Button color='green' inverted size='big' className='end' onClick={() => {setDishReady(d.presenceId, d.courseNumber, d.dishId); setUpdate(new Date())}}>Pronto</Button>
							</Container>
						</Card.Content>
					</Card>
				)
			})}
			<Divider hidden/>
		</Container>
	)
}