import { Container, Message, Button, Form } from 'semantic-ui-react'

import { useState } from 'react';
import { cloneObject } from '../common/helpFunctions';
import { logAdmin } from '../common/api';
import { useHistory } from 'react-router';

export function AdminLogInPage() {
	const history = useHistory()

	const [credentials, setCredentials] = useState({username: "", password: ""})
	const [error, setError] = useState({active: false, message: ""})

	const updateCredentials = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};	
		setCredentials(Object.assign(cloneObject(credentials), result));
	}

	const submitFunction = (event) => {
		logAdmin(credentials).then(response => {
			switch (response.status) {
				case 400:
					setError({active: true, message: response.message})
					break;
				case 200:
					history.goBack()
					break
				default:
					break;
			}
		})
	}

  return (
		<Container>
			<Form onSubmit={submitFunction} error={error.active}>
				<Form.Input id='username' size='small' onChange={updateCredentials('username')} value={credentials.username} label='Username'/>
				<Form.Input id='password' size='small' onChange={updateCredentials('password')} value={credentials.password} label='Password' type='password'/>
				<Message warning error={error.active} content={error.message}/>
				<Button inverted color='blue' type='submit'>Accedi</Button>
			</Form>
		</Container>
  );
}