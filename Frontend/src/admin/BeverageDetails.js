import { useEffect, useState } from "react"
import { Accordion, Container, Header, Icon, Input, Button } from "semantic-ui-react"
import { createBeverageTransaction, getBeverageWarehouseProgress } from "../common/api"
import { cloneObject } from "../common/helpFunctions"
import { Charts, ChartContainer, ChartRow, YAxis, LineChart} from "react-timeseries-charts";
import { TimeSeries } from "pondjs";
import Resizable from "react-timeseries-charts/lib/components/Resizable";
import { useHistory } from "react-router";


export function BeverageDetails(props) {

	const history = useHistory()

	const [collapsed, setCollapsed] = useState(!props.open)
	const [beverageProgress, setBeverageProgress] = useState([])
	const [transactionQuantity, setTransactionQuantity] = useState()
	const [update, setUpdate] = useState(new Date())

	let data;
	let timeseries;
	if (beverageProgress.length !== 0) {
		data = {
			name: props.beverage.Nome,
			columns: ["time", "value"],
			points: beverageProgress.map(bp => {
				return [new Date(bp.DataOra.replace(" ", "T")), bp.Quantita]
			})
		}
		data.points.push([new Date(), props.beverage.QuantitaInMagazzino])
		timeseries = new TimeSeries(data);
	}

	useEffect(() => {
		if (!collapsed) {
			getBeverageWarehouseProgress(props.beverage.CodiceBevanda).then(res => {
				if (res.status === 200) {
					const result = []
					res.data.forEach((e, i) => {
						if (i !== 0) {
							const progressiveSum = +e.Quantita + +result[i-1].Quantita
							result.push(Object.assign(cloneObject(e), {Quantita: progressiveSum}))
						} else {
							result.push(e)
						}
					});
					setBeverageProgress(result);
				} else if (res.status === 401) {
					history.push(`/admin/login`)	
				}
			})
		}
	}, [collapsed, update, history, props])

	const createTransaction = (quantity) => {
		if (quantity !== 0) {
			createBeverageTransaction(props.beverage.CodiceBevanda, quantity).then(res => {
				if (res.status === 200) {
					setTransactionQuantity(0);
					updateBeverage(props.beverage.CodiceBevanda, quantity)
				} else {
					//TODO: errore
				}
			})
		}
	}

	const updateBeverage = (beverageId, quantity) => {
		props.setWarehouse(Object.assign(cloneObject(props.warehouse), { "Beverages": cloneObject(props.warehouse.Beverages).map(b => {
			if(b.CodiceBevanda === beverageId){
				const newQuantity = +b.QuantitaInMagazzino + +quantity
				return(Object.assign(cloneObject(b), {"QuantitaInMagazzino": newQuantity}))
			} else {
				return b
			}
		})}))
	}

	return (
		<>
			<Accordion.Title active={!collapsed} onClick={() => setCollapsed(!collapsed)} className='noWrap center'>
				<Icon name='dropdown' />
				<Container className='flex'><Header className='noMargin marginRight'>{`${props.beverage.Nome}:`}</Header><p className={props.beverage.QuantitaInMagazzino <= 0 ? 'red' : ''}>{` ${props.beverage.QuantitaInMagazzino} bottoglie`}</p></Container>
			</Accordion.Title>
			<Accordion.Content active={!collapsed} id={`Beverage-${props.beverage.CodiceBevanda}`} >
				{beverageProgress.length !== 0 ?
					<Resizable className='width90'>
						<ChartContainer timeRange={timeseries.range()} format="%Y-%m-%d %H:%M" timeAxisAngledLabels={true} timeAxisHeight={100}>
							<ChartRow height="200">
								<YAxis id="axis" label="Bottiglie" width="50" min={0} max={Math.max(...beverageProgress.map(o => o.Quantita), 0)} type="linear" format=""/>
								<Charts>
										<LineChart axis="axis" series={timeseries}/>
								</Charts>
							</ChartRow>
						</ChartContainer>
					</Resizable>
				: null}

				<Container>
						<Header as='h4'>Esegui transazione</Header>
						<Container className='noWrap'>
							<Button inverted disabled={transactionQuantity > props.beverage.QuantitaInMagazzino} color='red' onClick={() => {createTransaction(-transactionQuantity); setUpdate(new Date())}}>-</Button>
							<Input value={transactionQuantity} type='number' min={0} max={100000} onChange={(e) => setTransactionQuantity(e.target.value || 0)} label='bottiglie' labelPosition='right'/>
							<Button inverted color='green' onClick={() => {createTransaction(transactionQuantity); setUpdate(new Date()) }}>+</Button>
						</Container>
				</Container>

			</Accordion.Content>
		</>
	)
}