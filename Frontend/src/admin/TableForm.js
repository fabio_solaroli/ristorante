import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { Container, Form, Button, Message, Divider, Header } from "semantic-ui-react";
import { addNewTable, changeTableMaxPeople, getTable, removeTableFromRestaurant } from "../common/api";
import { cloneObject } from "../common/helpFunctions";

export function AddTableForm() {

	const history = useHistory()

	const [table, setTable] = useState({name: "", maxPeople: 0})
	const [error, setError] = useState({active: false, message: ""})

	const updateTable = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};
		setTable(Object.assign(cloneObject(table), result))
	}

	const handleSubmit = async (event) => {
		addNewTable(table).then(response => {
			switch (response.status) {
				case 201:
					history.replace('/admin/tables')
					break;
				case 401:
					history.replace(`/admin/login`)
					break;
				default:
					setError({active: true, message: response.message})
					break;
			}
		})
	}

	return(
		<Container>
			<Header as='h1'>Aggiungi tavolo</Header>
			<Form onSubmit={handleSubmit} error={error.active}>
				<Container>
					<Form.Input required id='name' size='small' onChange={updateTable('name')} value={table.name} label='Nome'/>
					<Message warning error={error.active} content={error.message}/>
					<Form.Input required id='maxPeople' size='small' onChange={updateTable('maxPeople')} value={table.maxPeople} label='Numero massimo di persone' type='number' min={1}/>
				</Container>
				<Divider hidden/>
				<Container className='flex space-between'>
					<Button basic color='grey' type='submit' onClick={(e) => {e.preventDefault(); history.goBack()}}>Annulla</Button>
					<Button positive type='submit'>Aggiungi</Button>
				</Container>
			</Form>
		</Container>
	)
}

export function ModifyTableForm() {

	const history = useHistory()
	
	const { tableId } = useParams()

	const [table, setTable] = useState({})

	useEffect(() => {
		getTable(tableId).then(res => {
			if (res.status === 200) {
				setTable(res.data)
			} else {
				//errore 404
			}
		})
	}, [tableId])

	const updateTable = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};
		setTable(Object.assign(cloneObject(table), result))
	}

	const handleSubmit = async (event) => {
		changeTableMaxPeople(tableId, table.MaxPosti).then(response => history.replace('/admin/tables'))
	}

	return(
		<Container>
			<Header as='h1'>Modifica numero persone</Header>
			<Form onSubmit={handleSubmit}>
				<Container>
					<Form.Input id='name' size='small' disabled value={table.Nome} label='Nome'/>
					<Form.Input required id='maxPeople' size='small' onChange={updateTable('MaxPosti')} value={table.MaxPosti} label='Numero massimo di persone' type='number' min={1}/>
				</Container>
				<Divider hidden/>
				<Container className='flex space-between'>
					<Button basic color='grey' type='submit' onClick={(e) => {e.preventDefault(); history.goBack()}}>Annulla</Button>
					<Button positive type='submit'>Modifica</Button>
				</Container>
			</Form>
			<Divider hidden/>
			<Button inverted fluid color='red' onClick={() => {removeTableFromRestaurant(table.CodiceTavolo); history.goBack()}}>Rimuovi dal ristorante</Button>
		</Container>
	)
}