import { useEffect, useState } from "react"
import { useHistory, useParams } from "react-router"
import { Accordion, Container, Divider, Header, Button, Tab, Menu } from "semantic-ui-react"
import { checkWarehouse } from "../common/api"
import { scrollToElm } from "../common/helpFunctions"
import { BeverageDetails } from "./BeverageDetails"
import { IngredientDetails } from "./IngredientDetails"

export function Warehouse() {

	const { ingredientId, beverageId } = useParams()

	const [animation, setAnimation] = useState()

	useEffect(() => {
		let container = null
		let elm = null
		if (ingredientId !== undefined) {
			container = document.getElementById(`ingredientsAccordion`)
			elm = document.getElementById(`Ingredient-${ingredientId}`)
		}
		if (beverageId !== undefined) {
			container = document.getElementById(`beveragesAccordion`)
			elm = document.getElementById(`Beverage-${beverageId}`)
		}
		if (elm !== null) {
			scrollToElm(container, elm)
		}
	}, [animation, beverageId, ingredientId])

	const history = useHistory()

	const [warehouse, setWarehouse] = useState({"Ingredients": [], "Beverages": []})

	useEffect(() => {
		checkWarehouse().then(res => {
			if (res.status === 200) {
				setWarehouse(res.data)
				setAnimation('both')
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [history])

	const panes= [
		{ menuItem: (<Menu.Item><Header as='h2'>Ingredienti</Header></Menu.Item>), render: () =>
			<Container className='column-scroll noWrap column'>
				<Divider hidden className='marginBottom'/>
				<Accordion id='ingredientsAccordion'>
					{warehouse.Ingredients.map((i, index) => {
						return (
							<IngredientDetails key={index} ingredient={i} warehouse={warehouse} setWarehouse={setWarehouse} open={i.CodiceIngrediente.toString() === ingredientId}/>
						)
					})}
				</Accordion>
				<Divider hidden/>
				<Container className='noWrap column'>
					<Button className='end' inverted color='blue' onClick={() => history.push('/admin/warehouse/addIngredient')}>Nuovo Ingrediente</Button>
				</Container>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
			</Container>
		},
		{ menuItem: (<Menu.Item><Header as='h2'>Bevande</Header></Menu.Item>), render: () => 
			<Container className='column-scroll'>
				<Container className='flex column'>
					<Divider hidden className='marginBottom'/>
					<Accordion id='beveragesAccordion'>
						{warehouse.Beverages.map((b, index) => {
							return (
								<BeverageDetails key={index} beverage={b} warehouse={warehouse} setWarehouse={setWarehouse} open={b.CodiceBevanda.toString() === beverageId}/>
							)
						})}
					</Accordion>
					<Divider hidden/>
					<Container className='noWrap column'>
						<Button className='end' inverted color='blue' onClick={() => history.push('/admin/warehouse/addBeverage')}>Nuova Bevanda</Button>
					</Container>
					<Divider hidden/>
					<Divider hidden/>
					<Divider hidden/>
					</Container>
			</Container>	
		}
	]

	return (
		<Tab
			menu={{ secondary: true, pointing: true }}
			defaultActiveIndex={beverageId !== undefined ? 1 : 0}
			className='height100'
			panes={panes}
		/>
		
	)
}