import { useEffect, useState } from "react";
import { Button, Container, Divider, Form, List, Placeholder } from "semantic-ui-react";
import { getActivePresences, getTables, startPresence } from "../common/api";
import { cloneObject } from '../common/helpFunctions';
import ReactToPrint from 'react-to-print';
import React, { useRef } from 'react';
import { ShortPresence } from "./ShortPresence";
import { useHistory } from "react-router";
var QRCode = require('qrcode.react');

export function ManagePresences() {

	const history = useHistory()

	const [presence, setPresence] = useState({nPeople: 1, table: null, presenceId: null})
	const [activePresences, setActivePresences] = useState([])
	const [tables, setTables] = useState([])
	const [separateTable, setSeparateTable] = useState(false)
	const [update, setUpdate] = useState(new Date())

	useEffect(() => {
		getActivePresences().then(res => {
			if (res.status === 200) {
				setActivePresences(res.data)
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [update, history])

	useEffect(() => {
		getTables().then(res => {
			if (res.status === 200) {
				setTables(res.data.filter(t => t.InRistorante))
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [update, history])
	
	const updatePresence = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value, presenceId: null};	
		setPresence(Object.assign(cloneObject(presence), result));
	}

	const resetPresence = () => {
		setPresence({nPeople: 1, table: null, presenceId: null})
	}

	const setPresenceId = (presenceId) => {
		setPresence(Object.assign(cloneObject(presence), {presenceId: presenceId}))
	}

	const updateTableDropDown = (value) => {
		setPresence(Object.assign(cloneObject(presence), {table: tables.find(t => t.CodiceTavolo === value)}))
	}

	const manageSubmit = (e) => {
		startPresence(presence).then(res => {
			if (res.status === 201) {
				setPresenceId(res.data.CodicePresenza);
				setUpdate(new Date())
			}
		})
	}
	
	const componentToPrint = useRef();

	return (
		<Container className='flex space-between height100'>
			<Container className='width50 column-scroll'>
				{ presence.presenceId == null ?
					<Placeholder fluid>
						<Placeholder.Image square size='big'/>
					</Placeholder>
				:
					<Container className='flex space-between column'>
						<div ref={componentToPrint}>
							<QRCode
								value={presence.presenceId.toString()}
								size={window.innerWidth*0.8/2}
							/>
						</div>
						<ReactToPrint
							trigger={() => <Button className='end' inverted color='blue'>Stampa</Button>}
							content={() => componentToPrint.current}
							onAfterPrint={() => {
								setUpdate(new Date())
								resetPresence()
							}}
						/>
					</Container>
				}
				<Divider hidden/>
				<Form onSubmit={manageSubmit} className='flex column'>
					<Form.Checkbox toggle label="Tavolo separato" onChange={(e, c) => setSeparateTable(c.checked)}/>
					<Form.Group className='noWrap'>
						<Form.Input
							inline required
							id='nPeople' type='number' size='small' value={presence.nPeople} label='Nº Persone' 
							min={1} max={presence.table?.PostiOccupati > 0 ? (presence.table?.MaxPosti - +presence.table?.PostiOccupati) : presence.table?.MaxPosti || Math.max(...tables.map(t=> t.PostiOccupati > 0 ? t.MaxPosti - +t.PostiOccupati : t.MaxPosti), 0)}
							onChange={updatePresence('nPeople')}/>
						<Form.Dropdown
							inline search selection required clearable
							id='table' label={"Tavolo"} placeholder='Tavolo' value={presence.table?.CodiceTavolo}
							options={tables.filter(t => t.PostiOccupati > 0 ? (separateTable ? false : (+t.PostiOccupati + +presence.nPeople) <= +t.MaxPosti) : presence.nPeople <= t.MaxPosti).map(t => ({key: t.CodiceTavolo, text: `${t.Nome} (${t.PostiOccupati}/${t.MaxPosti} posti)` , value: t.CodiceTavolo, label: { color: t.PostiOccupati > 0 ? (t.MaxPosti === +t.PostiOccupati ? 'red' : 'orange') : 'green', empty: true, circular: true}}))}
							onChange={(e, d) => updateTableDropDown(d.value)}/>
					</Form.Group>
					<Divider hidden/>
					<Button className='end' primary color='blue' type='submit'>Genera Nuova Presenza</Button>
				</Form>
			</Container>
			<Container className='width50 padding left height100'>
				<List className='column-scroll'>
					{ activePresences.map(p => {
						return (<ShortPresence key={p.CodicePresenza} presence={p} setUpdate={setUpdate}/>)
					})}
				</List>
			</Container>	
		</Container>
	);
}