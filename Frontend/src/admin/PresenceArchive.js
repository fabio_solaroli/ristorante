import { useEffect, useState } from "react"
import { useHistory } from "react-router"
import { Container, List } from "semantic-ui-react"
import { getAllPresences } from "../common/api"
import { ShortPresence } from "./ShortPresence"


export function PresenceArchive() {

	const history = useHistory()

	const [presences, setPresences] = useState([])
	const [update, setUpdate] = useState(new Date())

	useEffect(() => {
		getAllPresences().then(res => {
			if (res.status === 200) {
				setPresences(res.data)
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [update, history])

	return (
		<Container className='padding left height100'>
			<List className='column-scroll'>
				{ presences.map(p => {
					return (<ShortPresence key={p.CodicePresenza} presence={p} setUpdate={setUpdate}/>)
				})}
			</List>
		</Container>
	)
}