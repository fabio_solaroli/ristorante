import { useEffect, useState } from "react"
import { Accordion, Container, Header, Icon, Input, Button, Dropdown } from "semantic-ui-react"
import { createIngredintTransaction, getIngredientWarehouseProgress } from "../common/api"
import { cloneObject, formatQuantity } from "../common/helpFunctions"
import { Charts, ChartContainer, ChartRow, YAxis, LineChart, Resizable } from "react-timeseries-charts";
import { TimeSeries } from "pondjs";
import { useHistory } from "react-router";


export function IngredientDetails(props) {

	const history = useHistory()

	const [collapsed, setCollapsed] = useState(!props.open)
	const [ingredientProgress, setIngredientProgress] = useState([])
	const [measureUnit, setMeasureUnite] = useState(1)
	const [transactionQuantity, setTransactionQuantity] = useState()
	const [update, setUpdate] = useState(new Date())

	let data;
	let timeseries;
	if (ingredientProgress.length !== 0) {
		data = {
			name: props.ingredient.Nome,
			columns: ["time", "value"],
			points: ingredientProgress.map(ip => {
				return [new Date(ip.DataOra.replace(" ", "T")), ip.Quantita]
			})
		}
		data.points.push([new Date(), props.ingredient.QuantitaInMagazzino])
		timeseries = new TimeSeries(data);
	}

	useEffect(() => {
		if (!collapsed) {
			getIngredientWarehouseProgress(props.ingredient.CodiceIngrediente).then(res => {
				if (res.status === 200) {
					const result = []
					res.data.forEach((e, i) => {
						if (i !== 0) {
							const progressiveSum = +e.Quantita + +result[i-1].Quantita
							result.push(Object.assign(cloneObject(e), {Quantita: progressiveSum}))
						} else {
							result.push(e)
						}
					});
					setIngredientProgress(result);
				} else if (res.status === 401) {
					history.push(`/admin/login`)	
				}
			})
		}
	}, [collapsed, update, history, props])

	const createTransaction = (quantity) => {
		if (quantity !== 0) {
			createIngredintTransaction(props.ingredient.CodiceIngrediente, quantity*measureUnit).then(res => {
				if (res.status === 200) {
					setTransactionQuantity(0);
					updateIngredient(props.ingredient.CodiceIngrediente, quantity*measureUnit)
				} else {
					//TODO: errore
				}
			})
		}
	}

	const updateIngredient = (ingredientId, quantity) => {
		props.setWarehouse(Object.assign(cloneObject(props.warehouse), { "Ingredients": cloneObject(props.warehouse.Ingredients).map(i => {
			if(i.CodiceIngrediente === ingredientId){
				const newQuantity = +i.QuantitaInMagazzino + +quantity
				return(Object.assign(cloneObject(i), {"QuantitaInMagazzino": newQuantity}))
			} else {
				return i
			}
		})}))
	}

	const options = [
		{key: 'g', text: 'g', value: 1},
		{key: 'kg', text: 'kg', value: 1000}
	]

	return (
		<>
			<Accordion.Title active={!collapsed} onClick={() => setCollapsed(!collapsed)} className='noWrap center'>
				<Icon name='dropdown' />
				<Container className='noWrap end'><Header className='noMargin marginRight'>{`${props.ingredient.Nome}: `}</Header><p className={props.ingredient.QuantitaInMagazzino <= 0 ? 'red' : ''}>{` ${formatQuantity(props.ingredient.QuantitaInMagazzino, 'solid')}`}</p></Container>
			</Accordion.Title>
			<Accordion.Content active={!collapsed} id={`Ingredient-${props.ingredient.CodiceIngrediente}`} >
				{ingredientProgress.length !== 0 ?
					<Resizable className='width90'>
						<ChartContainer timeRange={timeseries.range()} format="%Y-%m-%d %H:%M" timeAxisAngledLabels={true} timeAxisHeight={100}>
							<ChartRow height="200">
								<YAxis id="axis" label="Grammi" width="60" min={0} max={Math.max(...ingredientProgress.map(o => o.Quantita), 0)} type="linear" format=""/>
								<Charts>
										<LineChart axis="axis" series={timeseries}/>
								</Charts>
							</ChartRow>
						</ChartContainer>
					</Resizable>
				: null}

				<Container>
						<Header as='h4'>Esegui transazione</Header>
						<Container className='noWrap'>
							<Button inverted disabled={transactionQuantity*measureUnit > props.ingredient.QuantitaInMagazzino} color='red' onClick={() => {createTransaction(-transactionQuantity); setUpdate(new Date())}}>-</Button>
							<Input
								value={transactionQuantity} type='number' min={0} max={100000} step={measureUnit === 1 ? 1 : 0.001}
								onChange={(e) => setTransactionQuantity(e.target.value || 0)}
								label={<Dropdown
									options={options}
									value={measureUnit}
									onChange={(e, d) => setMeasureUnite(d.value)}/>}
								labelPosition='right'/>
							<Button inverted color='green' onClick={() => {createTransaction(transactionQuantity); setUpdate(new Date())}}>+</Button>
						</Container>
				</Container>

			</Accordion.Content>
		</>
	)
}