import { useEffect, useState } from "react";
import { Switch, Route, useHistory, Redirect } from "react-router-dom"
import { Menu, Icon, Sidebar, Dimmer } from 'semantic-ui-react'
import { checkAdminLoggedIn, logOutAdmin } from "../common/api";
import { AddBeverageForm } from "./AddBeverageForm";
import { AddDishForm } from "./AddDishForm";
import { AddIngredientForm } from "./AddIngredientForm";
import { AdminLogInPage } from "./AdminLogInPage";
import { AdminMenu } from "./Menu";
import { Warehouse } from "./Warehouse";
import { ManagePresences } from "./ManagePresences";
import { PresenceArchive } from "./PresenceArchive";
import { ManageTables } from "./ManageTables";
import { AddTableForm, ModifyTableForm } from "./TableForm";
import { Kitchen } from "./Kitchen";
import { PresenceDetails } from "./PresenceDetails";

export function AdminPage() {

	const history = useHistory()

	const [visible, setVisible] = useState(false);

	useEffect(() => {
		checkAdminLoggedIn().then(response => {
			if (!response.data) {
				history.push(`/admin/login`);
			}
		})
	}, [history.location.pathname, history.length, history])

	return (
		<Sidebar.Pushable>
			<Sidebar as={Menu}
				className='height100 width50'
				animation='overlay'
				icon='labeled'
				inverted
				vertical
				visible={visible}
			>
				<Menu.Item onClick={() => { history.push(`/admin/managePresences`); setVisible(false) }}>
					<Icon name='qrcode'/>
					Gestisci Presenze
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/admin/tables`); setVisible(false) }}>
					<Icon name='square'/>
					Gestisci Tavoli
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/admin/menu`); setVisible(false) }}>
					<Icon name='food'/>
					Menu
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/admin/kitchen`); setVisible(false) }}>
					<Icon name='utensil spoon'/>
					Cucina
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/admin/warehouse`); setVisible(false) }}>
					<Icon name='warehouse'/>
					Magazzino
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/admin/presences`); setVisible(false) }}>
					<Icon name='archive'/>
					Archivio Presenze
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/admin/login`); setVisible(false); logOutAdmin() }}>
					<Icon name='sign-out'/>
					Logout
				</Menu.Item>
			</Sidebar>

			<Sidebar.Pusher className='height100'>
				<Dimmer.Dimmable dimmed={visible} onClick={() => {if (visible) {setVisible(false)}}} className='fitContainer height100'>
						<Menu>
							<Menu.Item onClick={() => setVisible(!visible)}>
								<Icon name='sidebar'/>
							</Menu.Item>	
						</Menu>
						<Switch>
							<Route exact path={`/admin`}><Redirect to={`/admin/managePresences`}/></Route>
							<Route exact path={`/admin/login`}><AdminLogInPage/></Route>
							<Route exact path={`/admin/managePresences`}><ManagePresences/></Route>
							<Route exact path={`/admin/presences`}><PresenceArchive/></Route>
							<Route exact path={`/admin/presences/:presenceId`}><PresenceDetails/></Route>
							<Route exact path={`/admin/tables`}><ManageTables/></Route>
							<Route exact path={`/admin/tables/addTable`}><AddTableForm/></Route>
							<Route exact path={`/admin/tables/modifyTable/:tableId`}><ModifyTableForm/></Route>
							<Route exact path={`/admin/kitchen`}><Kitchen/></Route>
							<Route exact path={`/admin/menu`}><AdminMenu/></Route>
							<Route exact path={`/admin/menu/dish/:dishId`}><AdminMenu/></Route>
							<Route exact path={`/admin/menu/beverage/:beverageId`}><AdminMenu/></Route>
							<Route exact path={`/admin/menu/addDish`}><AddDishForm/></Route>
							<Route exact path={`/admin/menu/addDish/:categoryId`}><AddDishForm/></Route>
							<Route exact path={`/admin/warehouse`}><Warehouse/></Route>
							<Route exact path={`/admin/warehouse/ingredient/:ingredientId`}><Warehouse/></Route>
							<Route exact path={`/admin/warehouse/beverage/:beverageId`}><Warehouse/></Route>
							<Route exact path={`/admin/warehouse/addIngredient`}><AddIngredientForm/></Route>
							<Route exact path={`/admin/warehouse/addBeverage`}><AddBeverageForm/></Route>
							<Route exact path={`/admin/warehouse/addBeverage/:previousLocation`}><AddBeverageForm/></Route>
						</Switch>
						<Dimmer active={visible}/>
					</Dimmer.Dimmable>
				</Sidebar.Pusher>
		</Sidebar.Pushable>
	)
}