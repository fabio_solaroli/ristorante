import React, { useCallback, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { Button, Container, Divider, Form, Header, Icon, Input, List, Menu, Message, Segment, Tab } from "semantic-ui-react";
import { addNewCategory, editBeveragePrice, editDishPrice, getAdminAllBeverages, getAdminAllDishesByCategory, removeBeverageFromMenu, removeDishFromMenu, restoreBeverageInMenu, restoreDishInMenu } from "../common/api";
import { formatQuantity, groupArrayOfObjects, scrollToElm } from "../common/helpFunctions";

export function AdminMenu() {

	const { dishId, beverageId } = useParams()

	const [activeIndex, setActiveIndex] = useState(0)

	const animation = useCallback((index) => {
		const container = document.querySelector('.column-scroll')
		let elm = null
		if (dishId !== undefined) {
			elm = document.getElementById(`Dish-${dishId}`)
			setActiveIndex(index)
		}
		if (beverageId !== undefined) {
			elm = document.getElementById(`Beverage-${beverageId}`)
		}
		if (elm !== null) {
			scrollToElm(container, elm)
			elm.style='animation-duration: 1.5s; animation-name: outlineInAndOut; animation-delay: 0.6s'
		}
	}, [beverageId, dishId])

	const history = useHistory()

	const [dishesByCategory, setDishesByCategory] = useState([]);
	const [beverages, setBeverages] = useState([]);
	const [dishUpdate, setDishUpdate] = useState(new Date())
	const [beverageUpdate, setBeverageUpdate] = useState(new Date())
	const [newCategory, setNewCategory] = useState("")
	const [error, setError] = useState({active: false, message: ""})

	useEffect(() => {
		getAdminAllDishesByCategory().then(res => {
			if (res.status === 200) {
				const result = Object.values(groupArrayOfObjects(res.data, "CodiceCategoria")).map(c => {
					return ({
						id: c[0].CodiceCategoria,
						name: c[0].NomeCategoria,
						dishes: Object.values(groupArrayOfObjects(c, "CodicePiatto")).map(p => {
							if (p[0].CodicePiatto === null) {
								return null;
							} else {
								return ({
									id: p[0].CodicePiatto,
									name: p[0].Nome,
									price: p[0].PrezzoAttuale,
									preparationTime: p[0].TempoPreparazione,
									inMenu: p[0].InMenu,
									quantity: 0,
									ingredients: p.map( i => {
										return({
											id: i.CodiceIngrediente,
											name: i.NomeIngrediente,
											quantity: i.Quantita,
											warehouseQuantity: i.QuantitaInMagazzino
										})
									})
								})
							}
						})
					})
				}).map(c => c.dishes[0] === null ? Object.assign(c, {dishes: []}) : c)
				setDishesByCategory(result)
				animation(result.findIndex(c => c.dishes.find(d => d.id.toString() === dishId) !== undefined))
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})	
	}, [dishUpdate, history, dishId, animation])

	useEffect(() => {
		getAdminAllBeverages().then(res => {
			if (res.status === 200) {
				setBeverages(res.data)
				animation()
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [beverageUpdate, history, animation])

	const onSubmitNewCategory = async (e) => {
		if (newCategory !== '') {
			addNewCategory(newCategory).then(res => {
				switch (res.status) {
					case 201:
						setDishUpdate(new Date())
						setError({active: false, message: ""})
						break;
					default:
						setError({active: true, message: res.message})
						break;
				}
			})
		}
	}

	const secondaryPanes = dishesByCategory.map(category => { return(
		{ menuItem: <Menu.Item><Header as='h3'>{category.name}</Header></Menu.Item>, render: () => 
			<Container className='column-scroll'>
				<Divider hidden className='marginBottom'/>
				{category.dishes.map(dish => {
					return (
						<DishView key={`dish-${dish.id}`} dish={dish} setUpdate={setDishUpdate}/>
					);
				})}
				<Divider hidden/>
				<Container className='flex column'>
					<Button className='end' onClick={() => history.push(`/admin/menu/addDish/${category.id}`)} inverted color='blue'>Aggiungi Piatto</Button>
				</Container>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
			</Container>
		}
	)}).concat({ menuItem: <Menu.Item className='noWrap center'><Icon name='plus' color='green' size='small'/><Header color='green' className='noMargin' as='h3'>Nuova categoria</Header></Menu.Item> , render: () => 
		<Container className='marginTop'>
			<Divider hidden/>
			<Form onSubmit={onSubmitNewCategory} error={error.active} className='flex column'>
				<Form.Input className='width100' required value={newCategory} label='Nuova categoria' onChange={(e) => setNewCategory(e.target.value)}/>
				<Message warning error={error.active} content={error.message}/>
				<Button className='end noMargin' color='green' type='submit'>Crea Nuova Categoria</Button>
			</Form>
		</Container>
	})

	const handleTabChange = (e, { activeIndex }) => setActiveIndex(activeIndex)

	const panes= [
		{menuItem: <Menu.Item><Header as='h2'>Piatti</Header></Menu.Item>, render: () =>
			<Tab
				activeIndex={activeIndex}
				className='height100 marginTop row-scroll'
				panes={secondaryPanes}
				onTabChange={handleTabChange}
			/>
		},
		{menuItem: <Menu.Item><Header as='h2'>Bevande</Header></Menu.Item>, render: () => 
			<Container className='column-scroll'>
				<Divider hidden/>
				{beverages.length === 0 ?
					<p>Nessuna bevanda</p>
				: null}
				{beverages.map(beverage => {
					return (
						<BeverageView key={`beverage-${beverage.CodiceBevanda}`} beverage={beverage} setUpdate={setBeverageUpdate}/>
					)
				})}
				<Divider hidden/>
				<Container className='flex column'>
					<Button className='end' onClick={() => history.push(`/admin/warehouse/addBeverage/menu`)} inverted color='blue'>Aggiungi Bevanda</Button>
				</Container>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
			</Container>
		}
	]

	return (
		<Tab
			menu={{ secondary: true, pointing: true }}
			defaultActiveIndex={beverageId !== undefined ? 1 : 0}
			className='height100'
			panes={panes}
		/>
	);

}

function BeverageView(props) {
	
	const history = useHistory()

	const [price, setPrice] = useState(props.beverage.PrezzoAttuale)

	const isInMenu = () => {
		return props.beverage.InMenu;
	}

	const toggleBeverageInMenu = (e) => {
		if (isInMenu()) {
			removeBeverageFromMenu(props.beverage.CodiceBevanda)
		} else {
			restoreBeverageInMenu(props.beverage.CodiceBevanda)
		}
		props.setUpdate(new Date());
	}

	return (
		<Segment id={`Beverage-${props.beverage.CodiceBevanda}`}>
			<Container className='noWrap center marginBottom'>
				<Header as='h2' className='noMargin marginRight'>{props.beverage.Nome}</Header>
				<Icon name='warehouse'link onClick={() => history.push(`/admin/warehouse/beverage/${props.beverage.CodiceBevanda}`)}/>
			</Container>
			<Container>
				<Container className='flex vertical'>
					<p className='end'>Capienza: {formatQuantity(props.beverage.Capienza, 'liquid')}</p>
				</Container>
				<Divider hidden/>
				<Container className='noWrap'>
					<Input  
						value={price} type='number' min={0} max={10000} step={0.01}
						label='€' labelPosition='right'
						onChange={(e) => setPrice(e.target.value)}/>
					<Button inverted color='green' onClick={() => {editBeveragePrice(props.beverage.CodiceBevanda, price); props.setUpdate(new Date())}}>Aggiorna</Button>
				</Container>
			</Container>
			<Divider hidden/>
			<Container className='flex column'>
				<Button size='tiny' className='end' inverted color={isInMenu() ? 'red' : 'green'} onClick={toggleBeverageInMenu}>{isInMenu() ? "Rimuovi dal menù" : "Reinserisci nel menù"}</Button>
			</Container>
		</Segment>
	)
}


function DishView(props) {

	const history = useHistory()

	const [price, setPrice] = useState(props.dish.price)

	const isInMenu = () => {
		return props.dish.inMenu;
	}

	const toggleDishInMenu = (e) => {
		if (isInMenu()) {
			removeDishFromMenu(props.dish.id)
		} else {
			restoreDishInMenu(props.dish.id)
		}
		props.setUpdate(new Date());
	}

	return (
		<Segment id={`Dish-${props.dish.id}`}>
			<Header as='h2'>{props.dish.name}</Header>
			<Container>
				<Container className='flex space-between'>
					<List>
						{props.dish.ingredients.map(i => {
							return (
								<List.Item className={i.quantity > i.warehouseQuantity ? 'red' : ''} key={i.id}>
									{`${i.name}: ${formatQuantity(i.quantity, 'solid')}`}
									<Icon name='warehouse'link onClick={() => history.push(`/admin/warehouse/ingredient/${i.id}`)}/>
								</List.Item>
							);
						})}
					</List>
					<p>Tempo di preparazione: {props.dish.preparationTime} minuti</p>
				</Container>
				<Divider hidden/>
				<Container className='noWrap'>
					<Input 
						value={price} type='number' min={0} max={10000} step={0.01}
						label='€' labelPosition='right'
						onChange={(e) => setPrice(e.target.value)}/>
					<Button inverted color='green' onClick={() => {editDishPrice(props.dish.id, price); props.setUpdate(new Date())}}>Aggiorna</Button>
				</Container>
			</Container>
			<Divider hidden/>
			<Container className='flex column'>
				<Button size='tiny' className='end' inverted color={isInMenu() ? 'red' : 'green'} onClick={toggleDishInMenu}>{isInMenu() ? "Rimuovi dal menù" : "Reinserisci nel menù"}</Button>
			</Container>
		</Segment>
	);
}