import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { Container, Form, Button, Message, Label, Dropdown, Divider, Icon, Segment, Header } from "semantic-ui-react";
import { addNewCategory, addNewDish, getCategories, getIngredients } from "../common/api";
import { cloneObject } from "../common/helpFunctions";

export function AddDishForm() {

	const history = useHistory()

	const { categoryId } = useParams()

	const [dish, setDish] = useState({name: "", price: 0, ingredients: [{id: 0, quantity: 0, unitMeasure: 1}], category: +categoryId || "", preparationTime: 0})
	const [ingredients, setIngredients] = useState([])
	const [categories, setCategories] = useState([])
	const [error, setError] = useState({active: false, message: ""})
	const [update, setUpdate] = useState(new Date())

	useEffect(() => {
		getCategories().then(res => {
			if (res.status === 200) {
				res.data.push({CodiceCategoria: '+', Nome: "Aggiungi nuova categoria"})
				setCategories(res.data)
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [update, history])

	useEffect(() => {
		getIngredients().then(res => {
			if (res.status === 200) {
				setIngredients(res.data)
			} else if (res.status === 401) {
				history.push(`/admin/login`)	
			}
		})
	}, [update, history])

	const categoryOptions = categories.map(c => ({key: c.CodiceCategoria, text: c.Nome , value: c.CodiceCategoria}))
	const ingredientOptions = ingredients.map(i => ({key: i.CodiceIngrediente, text: i.Nome , value: i.CodiceIngrediente}))

	const updateDish = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};
		setDish(Object.assign(cloneObject(dish), result))
	}

	const addIngredient = (ingredient) => {
		const newIngredients = cloneObject(dish.ingredients)
		newIngredients.push(ingredient)
		setDish(Object.assign(cloneObject(dish), {ingredients: newIngredients}))
	}

	const removeIngredient = (index) => {
		const newIngredients = cloneObject(dish.ingredients)
		newIngredients.splice(index, 1)
		setDish(Object.assign(cloneObject(dish), {ingredients: newIngredients}))
	}

	const updateIngredient = (fieldName, value, index) => {
		const newIngredients = cloneObject(dish.ingredients)
		newIngredients[index][fieldName] = value;
		setDish(Object.assign(cloneObject(dish), {ingredients: newIngredients}))
	}

	const updateCategoryDropDown = (value) => {
		setDish(Object.assign(cloneObject(dish), {category: value}))
	}
	
	const handleSubmit = async (event) => {
		addNewDish(dish).then(response => {
			switch (response.status) {
				case 201:
					history.replace(`/admin/menu/dish/${response.data}`)
					break;
				default:
					setError({active: true, message: response.message})
					break;
			}
		})
	}

	const options = [
		{key: 'g', text: 'g', value: 1},
		{key: 'kg', text: 'kg', value: 1000}
	]

	return(
		<Container className='column-scroll'>
			<Header as='h1'>Aggiungi Piatto</Header>
			<Form onSubmit={handleSubmit} error={error.active}>
				<Form.Input required id='name' size='small' onChange={updateDish('name')} value={dish.name} label='Nome'/>
				{	dish.category === '+' ?
					null
				:
					<Message warning error={error.active} content={error.message}/>
				}
				<Form.Input required id='price' size='small' onChange={updateDish('price')} value={dish.price} label='Prezzo' type='number' min={1} step={0.01}>
					<input />
					<Label>€</Label>
				</Form.Input>
				<Form.Input required id='time' size='small' onChange={updateDish('preparationTime')} value={dish.preparationTime} label='Tempo di preparazione' type='number' min={1}>
					<input />
					<Label>minuti</Label>
				</Form.Input>
				{ dish.category === '+' ?
					<AddNewCategorySubForm error={error} setError={setError} updateCategoryDropDown={updateCategoryDropDown} setUpdate={setUpdate}/>
				:
					<Form.Dropdown clearable required id='category' label={"Categoria"} placeholder='Categoria' search selection options={categoryOptions} value={dish.category} onChange={(e, d) => updateCategoryDropDown(d.value)}/>
				}
				<Segment className='noWrap column'>
					{dish.ingredients.map((i, index) => {
						return (
							<Container key={index}>
								<Container className='noWrap space-between' key={`${i.id} ${index}`}>
									<Container className='flex'>
										<Form.Dropdown clearable required id='ingredient' placeholder='Ingrediente' search selection options={ingredientOptions.filter(fi => !dish.ingredients.map(ingr => ingr.id === i.id ? null : ingr.id).includes(fi.key))} value={dish.ingredients[index].id} onChange={(e, d) => updateIngredient('id', d.value, index)}/>
										<Form.Input disabled={i.id === 0} onChange={(e) => updateIngredient('quantity', e.target.value, index)} value={i.quantity} type='number' min={0}>
											<input/>
											<Label>
												<Dropdown
													options={options}
													value={i.unitMeasure}
													onChange={(e, d) => updateIngredient('unitMeasure', d.value, index)}
												/>
											</Label>
										</Form.Input>
									</Container>
									<Icon size='large' onClick={(e) => {e.preventDefault(); e.stopPropagation(); removeIngredient(index)}} name='close' color='red'/>
								</Container>
								<Divider hidden/>
							</Container>
						)
					})}
					<Divider hidden/>
					<Button
						className='end' inverted color='green' size='small'
						onClick={(e) => {
							e.preventDefault(); e.stopPropagation();
							addIngredient({id: 0, quantity: 0, unitMeasure: 1})
						}}
					>	
						Aggiungi ingrediente
					</Button>
				</Segment>
				<Divider hidden/>
				<Container className='flex space-between'>
					<Button basic color='grey' type='submit' onClick={(e) => {e.preventDefault(); history.goBack()}}>
						Annulla
					</Button>
					<Button positive type='submit'>Aggiungi</Button>
				</Container>
			</Form>
		</Container>
	)
}

function AddNewCategorySubForm(props) {
	const [newCategory, setNewCategory] = useState("");

	const handleClick = async (e) => {
		e.preventDefault()
		if (newCategory !== '') {
			addNewCategory(newCategory).then(res => {
				switch (res.status) {
					case 201:
						props.setUpdate(new Date())
						props.setError({active: false, message: ""})
						props.updateCategoryDropDown(res.data)
						break;
					default:
						props.setError({active: true, message: res.message})
						break;
				}
			})
		}
	}

	return (
		<Segment>
			<Container className='flex space-between'>
				<Header>Nuova Categoria</Header>
				<Icon size='large' color='red' name='close' onClick={() => {props.updateCategoryDropDown(""); props.setError({active: false, message: ""})}}/>
			</Container>
			<Container className='flex'>
				<Form.Input className='width100' required value={newCategory} onChange={(e) => setNewCategory(e.target.value)}>
					<input/>
					<Button color='green' onClick={handleClick}>Crea Nuova Categoria</Button>
				</Form.Input>
			</Container>
			<Message warning error={props.error.active} content={props.error.message}/>
		</Segment>
	)
}