import { useEffect, useState } from "react"
import { useHistory } from "react-router"
import { Container, List } from "semantic-ui-react"
import { checkUserLoggedIn, getAllUserPresences } from "../common/api"
import { ShortPresence } from "./ShortPresence"


export function Presences() {

	const history = useHistory()

	const [presences, setPresences] = useState([])
	const [update, setUpdate] = useState(new Date())

	useEffect(() => {
		checkUserLoggedIn().then(response => {
			if (response.status === 200) {
				if (response.data !== null) {
					getAllUserPresences(response.data).then(res => {
						if (res.status === 200) {
							setPresences(res.data)
						} else if (res.status === 401) {
							history.push(`/user/login`)	
						}
					})
				} else {
					history.push(`/user/login`)	
				}
			}
		})
	}, [update, history])

	return (
		<Container className='padding left height100'>
			<List className='column-scroll'>
				{ presences.map(p => {
					return (<ShortPresence key={p.CodicePresenza} presence={p} setUpdate={setUpdate}/>)
				})}
			</List>
		</Container>
	)
}