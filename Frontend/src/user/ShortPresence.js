import { useHistory } from "react-router";
import { Container, Header, List, Segment, Button, Divider } from "semantic-ui-react";
import { endPresence } from "../common/api";

export function ShortPresence(props) {

	const history = useHistory()

	return (
		<List.Item>
			<Segment className='flex column' color='blue' onClick={() => history.push(`/presence/${props.presence.CodicePresenza}`)} >
					<Container className='flex column' textAlign='right'>
						<p className='noMargin'>{`Arrivo: ${props.presence.DOArrivo}`}</p>
						<p>{new Date(props.presence.DOUscita.replace(" ", "T")) < new Date() ?
							`Uscita: ${props.presence.DOUscita}`
						: null}</p>
					</Container>
					<Divider className='noMargin marginBottom' hidden/>
					<Header as='h2' textAlign='center'>
						{ props.presence.EmailUtente !== null ?
								`${props.presence.Nome} ${props.presence.Cognome}`
						:
							"Ospite"
						}
					</Header>
					<Divider/>
					<Container className='flex column space-between'>
						<Container className='flex space-between'><Header sub>Numero Persone:</Header>{props.presence.NumeroPersone}</Container>
						<Container className='flex space-between'><Header sub>Tavolo:</Header>{props.presence.NomeTavolo}</Container>
						<Container className='flex space-between'><Header sub>Spesa:</Header>{props.presence.Spesa} €</Container>
					</Container>
					{new Date(props.presence.DOUscita.replace(" ", "T")) > new Date() ?
						<>
							<Divider hidden/>
							<Button inverted color='red' className='end' onClick={(e) => {
								e.preventDefault()
								e.stopPropagation()
								endPresence(props.presence.CodicePresenza);
								props.setUpdate(new Date())
							}}>Termina presenza</Button>
						</>
					: null}
			</Segment>
		</List.Item>
	);
}