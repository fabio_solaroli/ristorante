import { Container, Message, Button, Form, Divider } from 'semantic-ui-react'
import { postUser } from '../common/api';
import { useState } from 'react';
import { cloneObject } from '../common/helpFunctions';
import { Link, useHistory } from 'react-router-dom';

export function UserSignUp() {

	const history = useHistory()

	const [user, setUser] = useState({email: "", password: "", name: "", surname: ""})
	const [error, setError] = useState({active: false, message: ""})

	const updateUser = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};	
		setUser(Object.assign(cloneObject(user), result));
	}

	const submitFunction = (event) => {
		postUser(user).then(res => {
			if (res.status === 200) {
				history.push(`/home`)
			} else {
				setError({active: true, message: res.message})
			}
		})
	}

  return (
    <Container>
			<Form onSubmit={submitFunction} error={error.active}>
				<Form.Input id='email' size='small' onChange={updateUser('email')} value={user.email} label='Email' type='email' required/>
				<Form.Input id='password' size='small' onChange={updateUser('password')} value={user.password} label='Password' type='password' required/>
				<Form.Input id='name' size='small' onChange={updateUser('name')} value={user.name} label='Nome' required/>
				<Form.Input id='surname' size='small' onChange={updateUser('surname')} value={user.surname} label='Cognome' required/>
				<Message warning error={error.active} content={error.message}/>
				<Divider hidden className='noMargin marginBottom'/>
				<Button fluid className='noMargin' positive type='submit'>Registrati</Button>
			</Form>
			<Link to='/user/registration'></Link>
    </Container>
  );
}