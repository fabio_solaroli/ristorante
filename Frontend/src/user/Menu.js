import React, { useCallback, useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import { Link } from "react-router-dom";
import { Button, Container, Divider, Header, Icon, Input, List, Menu, Segment, Tab } from "semantic-ui-react";
import { getAllBeverages, getAllDishesByCategory, isPresenceActive, orderCourse } from "../common/api";
import { cloneObject, groupArrayOfObjects, scrollToElm } from "../common/helpFunctions";

export function RestaurantMenu() {

	const { dishId, beverageId } = useParams()

	const [activeIndex, setActiveIndex] = useState(0)

	const animation = useCallback((index) => {
		const container = document.querySelector('.column-scroll')
		let elm = null
		if (dishId !== undefined) {
			elm = document.getElementById(`Dish-${dishId}`)
			setActiveIndex(index)
		}
		if (beverageId !== undefined) {
			elm = document.getElementById(`Beverage-${beverageId}`)
		}
		if (elm !== null) {
			scrollToElm(container, elm)
			elm.style='animation-duration: 1.5s; animation-name: outlineInAndOut; animation-delay: 0.6s'
		}
	}, [beverageId, dishId])

	const history = useHistory()

	const [dishesByCategory, setDishesByCategory] = useState([]);
	const [presence, setPresence]= useState();
	const [beverages, setBeverages] = useState([]);
	const [reset, setReset] = useState(new Date());

	useEffect(() => {
		isPresenceActive().then(res => {
			if (res.status !== 200 || res.data === null) {
				history.push(`/home`)
			}	else {
				setPresence(res.data)
			}
		})
	}, [history])

	useEffect(() => {
		getAllDishesByCategory().then(res => {
			if (res.status === 200) {
				const result = Object.values(groupArrayOfObjects(res.data, "CodiceCategoria")).map(c => {
					return ({
						id: c[0].CodiceCategoria,
						name: c[0].NomeCategoria,
						dishes: Object.values(groupArrayOfObjects(c, "CodicePiatto")).map(p => {
							return ({
								id: p[0].CodicePiatto,
								name: p[0].Nome,
								price: p[0].PrezzoAttuale,
								inMenu: p[0].InMenu,
								quantity: 0,
								ingredients: p.map( i => {
									return({
										id: i.CodiceIngrediente,
										name: i.NomeIngrediente,
										quantity: i.Quantita,
										warehouseQuantity: i.QuantitaInMagazzino
									})
								})
							})
						})
					})
				})
				setDishesByCategory(result)
				animation(result.findIndex(c => c.dishes.find(d => d.id.toString() === dishId) !== undefined))
			}
		})	
	}, [reset, animation, dishId])

	useEffect(() => {
		getAllBeverages().then(res => {
			if (res.status === 200) {
				setBeverages(res.data.map(b => Object.assign(b, {quantity: 0})))
				animation()
			}
		})
	}, [reset, animation])

	const updateBeverageQuantity = (index) => (quantity) => {
		const updatedBeverages = cloneObject(beverages)
		updatedBeverages[index].quantity += (quantity);
		setBeverages(updatedBeverages)
	}

	const updateDishQuantity = (categoryIndex, dishIndex) => (quantity) => {
		const updatedDishes = cloneObject(dishesByCategory);
		updatedDishes[categoryIndex].dishes[dishIndex].quantity += (quantity);

		updatedDishes[categoryIndex].dishes[dishIndex].ingredients.forEach(ingredient => {
			updatedDishes.forEach((c, ci) => {
				c.dishes.forEach((d, di) => {
					d.ingredients.forEach((i, ii) => {
						if (i.id === ingredient.id) {
							updatedDishes[ci].dishes[di].ingredients[ii].warehouseQuantity += (-quantity*(ingredient.quantity))
						}
					})
				})
			})
		})

		setDishesByCategory(updatedDishes)
	}

	const secondaryPanes = dishesByCategory.map((category, cIndex) => { return (
		{ menuItem: <Menu.Item><Header as='h3'>{category.name}</Header></Menu.Item>, render: () => 
			<Container className='column-scroll height80'>
				<Divider hidden className='marginBottom'/>
				{category.dishes.map((dish, dIndex) => {
					return (
						<DishView key={dish.id} dish={dish} updateDishQuantity={updateDishQuantity(cIndex, dIndex)}/>
					);
				})}
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
				<Divider hidden/>
			</Container>
		}
	)})

	const handleTabChange = (e, { activeIndex }) => setActiveIndex(activeIndex)

	const panes= [
		{menuItem: <Menu.Item><Header as='h2'>Piatti</Header></Menu.Item>, render: () =>
			<Tab
				activeIndex={activeIndex}
				className='height100 marginTop row-scroll'
				panes={secondaryPanes}
				onTabChange={handleTabChange}
			/>
		},
		{menuItem: <Menu.Item><Header as='h2'>Bevande</Header></Menu.Item>, render: () => 
			<Container className='column-scroll height80'>
				<Divider hidden/>
				{beverages.length === 0 ?
					<p>Nessuna bevanda</p>
				: null}
				{beverages.map((beverage, index) => {
					return (
						<BeverageView key={`beverage-${index}`} beverage={beverage} updateBeverageQuantity={updateBeverageQuantity(index)}/>
					)
				})}
				<Divider hidden/>
			</Container>
		}
	]


	return (<>
		<Tab
			menu={{ secondary: true, pointing: true }}
			defaultActiveIndex={beverageId !==undefined ? 1 : 0}
			className='height100'
			panes={panes}
		/>

		<Container className='fixed'>
			<Divider/>
			<Container className='noWrap space-between height60'>
				<Container className='width50 height80'>
					<Header>
						Recap Ordinazione: { dishesByCategory.map(c => {
								return (c.dishes.map(d => { return(d.quantity * d.price) }).reduce(((a, b) => a + b), 0))
							}).reduce(((a, b) => a + b), 0)
							+ beverages.map(b => b.PrezzoAttuale * b.quantity).reduce(((a, b) => a + b), 0)
						}€
					</Header>
					{window.innerWidth > 500 ?
						<Container className='column-scroll noWrap column noPadding'>
							{dishesByCategory.map(c => {
								return (c.dishes.filter(d => d.quantity > 0).map(d => {
										return (
											<Container key={`dish-${d.id}`} className='width100'>
												<p>{d.name} × {d.quantity}: {d.price*d.quantity}€</p>
												<Divider/>
											</Container>
										)
								}))
							})}
							{beverages.filter(b => b.quantity !== 0).map(b => {
								return(
									<Container key={`beverage-${b.CodiceBevanda}`} className='width100'>
										<p>{b.Nome} × {b.quantity}: {b.PrezzoAttuale*b.quantity}€</p>
										<Divider/>
									</Container>
								)
							})}
						</Container>
					:null}
				</Container>
				<Container className='noWrap column width50 height100'>
					<Container className='flex justify-center'>
						<Button
							disabled={
								dishesByCategory.map(c => {
									return (c.dishes.map(d => { return(d.quantity * d.price) }).reduce(((a, b) => a + b), 0))
								}).reduce(((a, b) => a + b), 0)
								+ beverages.map(b => b.PrezzoAttuale * b.quantity).reduce(((a, b) => a + b), 0) === 0
							}
							size='massive'
							inverted
							color='blue'
							onClick={() => {		
								orderCourse(
									dishesByCategory.flatMap(category => {
										return (category.dishes.filter(dish => dish.quantity > 0).map(dish => {
											return({CodicePiatto: dish.id, Quantita: dish.quantity})
										}))
									}).filter(e => e !== undefined),
									beverages.filter(b => b.quantity > 0).map(b => {return {CodiceBevanda: b.CodiceBevanda, Quantita: b.quantity}})
								).then(res => {
									if (res.status === 201) {
										setReset(new Date())
									}
								})
							}}>
								Ordina
						</Button>
					</Container>
					<Container className='flex justify-center'>
						<Link className='padding top' to={ presence !== undefined ? `/presence/${presence.CodicePresenza}` : ''}>Dettagli presenza</Link>
					</Container>
				</Container>
			</Container>
		</Container>
	</>);

}

function BeverageView(props) {
	return (
		<Segment id={`Beverage-${props.beverage.CodiceBevanda}`}>
			<Header as='h2'>{props.beverage.Nome}</Header>
			<Container className='flex space-between'>
				<Container>
					<p className='end'>Capienza: {props.beverage.Capienza} ml</p>
				</Container>
				<Divider hidden/>
				<Container>
					{props.beverage.PrezzoAttuale} €
					<Divider hidden/>
				</Container>
				<Container className='noWrap space-around height100 selfCenter'>
					<Icon size='large' disabled={props.beverage.quantity <= 0} circular name='minus' color='red' onClick={() => props.updateBeverageQuantity(-1)}/>
					<Input value={props.beverage.quantity} className='width50'/>
					<Icon size='large' disabled={props.beverage.quantity === props.beverage.QuantitaInMagazzino} circular name='plus' color='green' onClick={() => props.updateBeverageQuantity(1)}/>
				</Container>
			</Container>
			<Divider hidden/>
		</Segment>
	)
}


function DishView(props) {
	return (
		<Segment id={`Dish-${props.dish.id}`}>
			<Header as='h2'>{props.dish.name}</Header>
			<Container className='flex space-between'>
				<Container>
					<List>
						{props.dish.ingredients.map(i => {
							return (<List.Item key={i.id}>{`${i.name}`}</List.Item>);
						})}
					</List>
					<Divider hidden/>
				</Container>
				<Container>
					{props.dish.price} €
				</Container>
				<Container className='noWrap space-around height100 selfCenter'>
					<Icon size='large' disabled={props.dish.quantity <= 0} circular name='minus' color='red' onClick={() => props.updateDishQuantity(-1)}/>
					<Input value={props.dish.quantity} className='width50'/>
					<Icon size='large' disabled={props.dish.ingredients.map(i => i.quantity > i.warehouseQuantity).reduce(((a, b) => a || b), false)} circular name='plus' color='green' onClick={() => props.updateDishQuantity(1)}/>
				</Container>
			</Container>
			<Divider hidden/>
		</Segment>
	);
}