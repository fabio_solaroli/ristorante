import { useEffect, useState } from "react";
import { useHistory } from "react-router";
import { Button, Container, Header, Icon } from "semantic-ui-react";
import { checkUserLoggedIn, getUserInformation, logOutUser } from "../common/api";

export function UserProfile() {

	const history = useHistory()

	const [user, setUser] = useState({})

	// const [newPassword, setNewPassword] = useState("") //TODO: change password

	useEffect(() => {
		checkUserLoggedIn().then(res => {
			if (res.status === 200) {
				if (res.message) {
					getUserInformation(res.data).then(res1 => {
						if (res1.status === 200) {
							setUser(res1.data)
						}
					})
				} else {
					history.push('/user/login')					
				}
			}
		})
	}, [history])

	return(
		<Container className='flex column'>
			<Header as='h1'>{`${user.Nome} ${user.Cognome}`}</Header>
			<Container className='noWrap'>
				<Header as='h3'>Email:</Header>
				<p className='marginLeft'>{user.Email}</p>
			</Container>
			<Button inverted color='red' className='end' onClick={() => {logOutUser(); setUser({}); history.push('/home')}}>
				<Icon name='sign-out'/>
				Logout
			</Button>
			{/* <Divider/> */}
			{/* <Form>
				<Form.Input  label='Nuova password' type='password' required/>
				<Form.Input label='Conferma nuova password' type='password' required/>
				<Button type='submit'>Cambia password</Button>
			</Form> */}
		</Container>
	);
}