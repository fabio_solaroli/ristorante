import { Container, Message, Button, Form, Icon, Divider } from 'semantic-ui-react'

import { useState } from 'react';
import { cloneObject } from '../common/helpFunctions';
import { useHistory } from 'react-router-dom';
import { logUser } from '../common/api';

export function UserLogIn() {

	const history = useHistory();

	const [credentials, setCredentials] = useState({email: "", password: ""})
	const [error, setError] = useState({active: false, message: ""})

	const updateCredentials = (fieldName) => (event) => {
		const result = {[fieldName]: event.target.value};	
		setCredentials(Object.assign(cloneObject(credentials), result));
	}

	const submitFunction = (event) => {
		logUser(credentials).then(response => {
			switch (response.status) {
				case 200:
					history.replace('/home')
					break
				default:
					setError({active: true, message: response.message})
					break;
			}
		})
	}

  return (
    <Container className='flex column'>
			<Form className='flex column' onSubmit={submitFunction} error={error.active}>
				<Form.Input id='email' size='small' onChange={updateCredentials('email')} value={credentials.email} label='Email' type='email'/>
				<Message warning error={error.active} content={error.message}/>
				<Form.Input id='password' size='small' onChange={updateCredentials('password')} value={credentials.password} label='Password' type='password'/>
				<Divider hidden className='noMargin marginBottom'/>
				<Button className='noMargin' positive type='submit'>Accedi</Button>
			</Form>
			<Divider hidden/>
			<Button inverted size='small' className='end' as='a' color='blue' onClick={() => history.push('/user/registration')}>
				<Icon name = "signup"/>
				Registrati
			</Button>
    </Container>
  );
}