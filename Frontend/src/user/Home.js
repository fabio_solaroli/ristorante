import { Button, Container, Divider, Header, Message } from "semantic-ui-react";
import QrReader from 'react-qr-reader'
import React, { useEffect, useState } from "react";
import { addUserToPresence, checkUserLoggedIn, getPresence, isPresenceActive } from "../common/api";
import { useHistory } from "react-router";

export function Home() {

	const history = useHistory()

	useEffect(() => {
		isPresenceActive().then(res => {
			if (res.status === 200 && res.data !== null) {
				history.push(`/menu`)
			}
		})
	}, [history])

	const [showReader, setShowReader] = useState(false)
	const [error, setError] = useState({active: false, message: ""})

	return (
		<Container className='noWrap column center column-scroll space-between'>
			<Container>
				<Header as='h1'>Benvento</Header>
				{ !showReader ?
					<Header textAlign='center' as='h2'>Clicca il pulsante per scannerizzare il codice di una presenza</Header>
				: null}
			</Container>
			{ showReader ? 
				<QrReader
					className='width100 marginBottom'
					delay={1}
					onError={(e) => console.log(e)}
					onScan={(data) => {
						if (data!= null) {
							const presenceId = data
							getPresence(data).then(res => {
								if (res.status === 200) {
									if (res.message !== 'HasUser') {
										checkUserLoggedIn().then(res1 => {
											if (res1.status === 200 && res1.data !== null) {
												addUserToPresence(res1.data, presenceId)
											}
										})
									}
									if (new Date(res.data.DOUscita.replace(" ", "T")) > new Date()) {
										history.push(`/menu`)
									} else {
										history.push(`/presence/${presenceId}`)
									}
								} else {
									setError({active: true, message: res.message})
								}
								setShowReader(false)
							})
						}
					}}
				/>
			:
				<Button size='huge' circular primary onClick={() => setShowReader(true)}>
					Scansiona Codice QR
				</Button>
			}
			{ showReader ?
					<Container><Button fluid circular basic color='grey' onClick={() => setShowReader(false)}>Annulla</Button></Container>
			: <Divider hidden/> }
			{ error.active && !showReader ?
				<Message className='floatingBottom' error={error.active} content={error.message}/>
			: null }
		</Container>
	);
}