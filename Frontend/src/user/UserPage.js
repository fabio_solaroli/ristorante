import { Switch, Route, useHistory, Redirect } from "react-router-dom"
import { Icon, Sidebar, Dimmer, Menu } from 'semantic-ui-react'
import { useEffect, useState } from 'react';
import { checkUserLoggedIn, getUserInformation, logOutUser } from "../common/api";
import { UserLogIn } from "./UserLogIn";
import { UserSignUp } from "./UserSignUp";
import { UserProfile } from "./UserProfile";
import { Home } from "./Home";
import { RestaurantMenu } from "./Menu"
import { PresenceDetails } from "./PresenceDetails";
import { Presences } from "./Presences";

export function UserPage() {

	const history = useHistory()	
	
	const [user, setUser] = useState(null)
	const [visible, setVisible] = useState(false);
	
	useEffect(() => {
		checkUserLoggedIn().then(response => {
			if (response.data !== null) {
				getUserInformation(response.data).then(r => {
					if (r.status === 200) {
						setUser(r.data)
					}
				})
			}
		})
	}, [history])

  return (
		<Sidebar.Pushable>
			<Sidebar as={Menu}
				className='height100 width50'
				animation='overlay'
				icon='labeled'
				inverted
				vertical
				visible={visible}
			>
				<Menu.Item name={"Home"} onClick={() => {history.push('/home'); setVisible(false)}}>
					<Icon name='home'/>
					Home
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/menu`); setVisible(false) }}>
					<Icon name='food'/>
					Menu
				</Menu.Item>
				<Menu.Item onClick={() => { history.push(`/presences`); setVisible(false) }}>
					<Icon name='archive'/>
					Presenze
				</Menu.Item>
				{user !== null ?
					<Menu.Item onClick={() => {history.push('/home'); setUser(null); setVisible(false); logOutUser()}}>
						<Icon name='sign-out'/>
						Logout
					</Menu.Item>
				: null}
			</Sidebar>

			<Sidebar.Pusher className='height100'>
				<Dimmer.Dimmable dimmed={visible} onClick={() => {if (visible) {setVisible(false)}}} className='fitContainer height100'>
						<Menu>
							<Menu.Item onClick={() => setVisible(!visible)}>
								<Icon name='sidebar'/>
							</Menu.Item>
							<Menu.Menu position='right'>
								<Menu.Item onClick={() => {
									checkUserLoggedIn().then(response => {
										if (response.data === null) {
											history.push(`/user/login`);
										} else {
											history.push(`/user/profile`)
										}
									})
								}}>
									<Icon name={user !== null ? 'user' : 'user outline'}/>
									{user !== null ? user.Nome : null}
								</Menu.Item>
							</Menu.Menu>
						</Menu>

						<Switch>
							<Route exact path="/"><Redirect to='/home'/></Route>
							<Route exact path="/home"><Home/></Route>
							<Route exact path="/menu"><RestaurantMenu/></Route>
							<Route exact path={`/menu/dish/:dishId`}><RestaurantMenu/></Route>
							<Route exact path={`/menu/beverage/:beverageId`}><RestaurantMenu/></Route>
							<Route exact path="/presences"><Presences/></Route>
							<Route exact path="/presence/:presenceId"><PresenceDetails/></Route>
							<Route exact path="/user/login"><UserLogIn/></Route>
							<Route exact path="/user/registration"><UserSignUp/></Route>
							<Route exact path="/user/profile"><UserProfile/></Route>
						</Switch>
						<Dimmer active={visible}/>
					</Dimmer.Dimmable>
				</Sidebar.Pusher>
		</Sidebar.Pushable>
  );
}
