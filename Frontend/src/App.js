import './App.css'
import { Switch, Route, BrowserRouter } from "react-router-dom"
import { AdminPage } from './admin/AdminPage';
import { UserPage } from './user/UserPage';

function App() {
  return (
		<BrowserRouter basename="/app">
			<Switch>
				<Route path="/admin"><AdminPage/></Route>
				<Route path='/'><UserPage/></Route>
			</Switch>
    </BrowserRouter>
  );
}
export default App;
